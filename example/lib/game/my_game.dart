import 'package:example/blocs/my_game_bloc.dart';
import 'package:example/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class MyGameView extends ConsumerWidget {
  const MyGameView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final myGameBloc = ref.watch(myGameBlocProvider);
    return Material(
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 100,
              width: 200,
              color: Colors.black,
              child: StreamBuilder<List<List<int>>>(
                stream: myGameBloc.array2DSubject.stream,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    // handle loading
                    return const Center(child: CircularProgressIndicator());
                  } else if (snapshot.hasData) {
                    // handle data
                    final item = snapshot.data!;
                    return Stack(
                      children: [
                        Positioned(
                          left: 0,
                          top: 0,
                          child: Container(
                            width: 50,
                            height: 50,
                            color:
                                (item[0][0] == 1) ? Colors.green : Colors.amber,
                          ),
                        ),
                        Positioned(
                          left: 50,
                          top: 0,
                          child: GestureDetector(
                            onTap: () {
                              myGameBloc.btnEvent.add(MyClickCarpetEvent(0, 1));
                            },
                            child: Container(
                              width: 50,
                              height: 50,
                              color: (item[0][1] == 1)
                                  ? Colors.green
                                  : Colors.amber,
                            ),
                          ),
                        ),
                      ],
                    );
                  } else if (snapshot.hasError) {
                    // handle error (note: snapshot.error has type [Object?])
                    final error = snapshot.error!;
                    return Text(error.toString());
                  } else {
                    // uh, oh, what goes here?
                    return Text('Some error occurred - help!');
                  }
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
