import 'package:example/blocs/my_game_bloc.dart';
import 'package:example/find_device_button.dart';
import 'package:example/game/my_game.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

void main() {
  runApp(const ProviderScope(child: MyApp()));
}

final myGameBlocProvider = Provider((ref) {
  return MyGameBloc();
});

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
      routes: {"myGameView": (context) => const MyGameView()},
    );
  }
}

class MyHomePage extends ConsumerWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    print("MyHomePage build");
    return Scaffold(
      appBar: AppBar(
        title: const Text("example"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const FindDevicesButton(),
            ElevatedButton(
                onPressed: () {
                  //go to next page
                  final myGameBloc = ref.read(myGameBlocProvider);
                  myGameBloc.init();
                  Navigator.of(context).pushNamed('myGameView');
                },
                child: const Text("進遊戲頁面")),
          ],
        ),
      ),
    );
  }
}
