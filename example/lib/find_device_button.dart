import 'package:flutter/material.dart';
import 'package:whiztoys_ble/widgets.dart';

class FindDevicesButton extends StatelessWidget {
  const FindDevicesButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.only(top: 60.0),
          child: ElevatedButton(
            onPressed: () async {
              var result = await showDialog(
                  context: context,
                  barrierDismissible: false,
                  builder: (context) {
                    return MediaQuery.removeViewPadding(
                      context: context,
                      removeRight: true,
                      removeBottom: true,
                      child: const FindDeviceDialog(),
                    );
                  });
            },
            child: const Text("連線狀態"),
          ),
        ),
      ),
    );
  }
}
