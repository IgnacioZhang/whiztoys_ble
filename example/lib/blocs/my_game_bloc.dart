import 'package:rxdart/rxdart.dart';
import 'package:whiztoys_ble/whiztoys_ble.dart';

class MyGameBloc {
  MyGameBloc() {
    btnEvent
        .debounceTime(const Duration(milliseconds: 50))
        .listen((event) => _btnObserver(event));
    WhizToysService.instance.sensorsStream
        .listen((sensors) => _onSensorChange(sensors));

    init();
  }
  final String _TAG = "MyGameBloc";

  PublishSubject<MyButtonEvent> btnEvent = PublishSubject();

  //想關聯到UI的東西、例如資料
  List<List<int>> _array2D = [[]];
  BehaviorSubject<List<List<int>>> array2DSubject =
      BehaviorSubject<List<List<int>>>.seeded([[]]);

  //初始化
  init() {
    print("$_TAG init");
    _array2D = [
      [0, 0, 0, 0],
      [0, 0, 0, 0]
    ];
    array2DSubject.sink.add(_array2D);
  }

  //當感測器數值改變時執行
  _onSensorChange(List<WhizToysSensor> sensors) {
    for (WhizToysSensor sensor in sensors) {
      if (sensor.leftTop == PressureStatus.P3) {
        print("left top pressed");
        _array2D[0][0] = 1;
      } else if (sensor.leftTop == PressureStatus.NORMAL) {
        _array2D[0][0] = 0;
      }
      if (sensor.rightTop == PressureStatus.P3) {
        print("right top pressed");
        _array2D[0][1] = 1;
      } else if (sensor.leftTop == PressureStatus.NORMAL) {
        _array2D[0][1] = 0;
      }
    }
    array2DSubject.sink.add(_array2D); //更新array2 觸發UI更新
  }

  //當按鈕點擊時執行（需使用btnEvent.add）
  _btnObserver(MyButtonEvent event) {
    if (event is MyClickCarpetEvent) {
      int x = event.x; //此時event已經被認定為是MyClickCarpetEvent型別
      int y = event.y;
      conrolLed(x, y, 255, 255, 0);
    } else if (event is MyShowLightEvent) {}
  }

  conrolLed(int x, int y, int r, int g, int b) {
    //寫死的寫法
    // final led = WhizToysLed.feedback(
    //     sensorRow: 0,
    //     sensorCol: 0,
    //     showLedImmediately: true,
    //     ledMode: LedMode.none,
    //     showLedLong: true,
    //     controlAllLedSep: true,
    //     R: 255,
    //     G: 0,
    //     B: 0);
    // WhizToysService.instance.ledControlSubject
    //     .add(led.ledPacket..insert(0, 0x03));

    //帶入參數x,y的寫法 ,R,G,B
    final led2 = WhizToysLed.feedback(
        sensorRow: x,
        sensorCol: y,
        showLedImmediately: true,
        ledMode: LedMode.none,
        showLedLong: true,
        controlAllLed: false,
        controlAllLedSep: false,
        R: r,
        G: g,
        B: b);
    WhizToysService.instance.ledControlSubject
        .add(led2.ledPacket..insert(0, 0x03));
  }
}

//以下為事件定義
abstract class MyButtonEvent {}

class MyShowLightEvent extends MyButtonEvent {}

class MyClickCarpetEvent extends MyButtonEvent {
  MyClickCarpetEvent(this.x, this.y);
  int x, y;
}
