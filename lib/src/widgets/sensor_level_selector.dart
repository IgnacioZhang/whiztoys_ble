import 'package:flutter/material.dart';

enum SensorLevelSelector { add, delete }

class SensorLevelSelectorWidget extends StatelessWidget {
  const SensorLevelSelectorWidget(
      {Key? key, required this.level, required this.onChange})
      : super(key: key);

  final int level;
  final Function(SensorLevelSelector) onChange;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 50,
      height: 150.0,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(
            "靈敏度",
            style: TextStyle(color: Colors.grey[600]),
          ),
          GestureDetector(
            onTap: () => onChange(SensorLevelSelector.add),
            child: const Icon(
              Icons.arrow_drop_up_rounded,
              color: Color(0xff97C9B2),
              size: 40,
            ),
          ),
          Container(
            height: 30.0,
            width: 40.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(4.0),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 40,
                  offset: Offset(0, 1),
                ),
              ],
            ),
            child: Center(
                child: Text(level == 1
                    ? "低"
                    : level == 2
                        ? "中"
                        : "高")),
          ),
          GestureDetector(
            onTap: () => onChange(SensorLevelSelector.delete),
            child: const Icon(
              Icons.arrow_drop_down_rounded,
              color: Color(0xff97C9B2),
              size: 40,
            ),
          )
        ],
      ),
    );
  }
}
