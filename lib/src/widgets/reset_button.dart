import 'package:flutter/material.dart';

class ResetButton extends StatefulWidget {
  const ResetButton({Key? key, required this.onTap}) : super(key: key);

  final Function onTap;

  @override
  State<ResetButton> createState() => _ResetButtonState();
}

class _ResetButtonState extends State<ResetButton>
    with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 2),
    vsync: this,
  )..repeat(reverse: false);

  late final Animation<double> _animation = CurvedAnimation(
    parent: _controller,
    curve: Curves.easeInOut,
  );

  @override
  void initState() {
    _controller.stop();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _controller.reset();
        _controller.forward();
        widget.onTap();
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('感測器校正', style: TextStyle(color: Colors.grey[600])),
          ),
          RotationTransition(
            turns: _animation,
            child: const SizedBox(
              width: 30,
              height: 30,
              child: Image(
                image: AssetImage(
                  'assets/icon_refresh.png',
                  package: "whiztoys_ble",
                ),
                width: 40,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
