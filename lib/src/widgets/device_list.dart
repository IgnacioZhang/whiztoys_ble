import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../widgets.dart';
import '../whiztoys/whiztoys_service.dart';

class DeviceListView extends StatelessWidget {
  const DeviceListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.symmetric(horizontal: 24.0),
      shrinkWrap: true,
      scrollDirection: Axis.vertical,
      children: [
        //已連線裝置
        StreamBuilder<List<BluetoothDevice>>(
          stream: Stream.periodic(const Duration(seconds: 2))
              .asyncMap((_) => FlutterBluePlus.instance.connectedDevices),
          initialData: [],
          builder: (c, snapshot) => Column(
            children: snapshot.data!
                .map((d) => d.name.isNotEmpty && (d.name.contains('WTS'))
                    ? GestureDetector(
                        child: DeviceTile(
                          title: d.name,
                          subtitle: Platform.isAndroid ? d.id.id : "",
                          connected: true,
                        ),
                        onTap: () {},
                        onLongPress: () {},
                      )
                    : SizedBox())
                .toList(),
          ),
        ),
        //掃描到裝置
        StreamBuilder<List<ScanResult>>(
          stream: FlutterBluePlus.instance.scanResults,
          initialData: [],
          builder: (c, snapshot) => Column(
            children: snapshot.data!
                .map((r) => r.device.name.isNotEmpty &&
                        r.device.name.contains('WTS')
                    ? GestureDetector(
                        child: DeviceTile(
                          title: r.device.name,
                          subtitle: Platform.isAndroid ? r.device.id.id : "",
                          connected: false,
                        ),
                        onTap: () async {
                          Fluttertoast.showToast(
                              msg: "藍牙連線中!請稍候...",
                              toastLength: Toast.LENGTH_LONG,
                              gravity: ToastGravity.CENTER,
                              backgroundColor: Theme.of(context).primaryColor,
                              textColor: Colors.white,
                              fontSize: 16.0);
                          WhizToysService.instance.device = r.device;

                          await WhizToysService.instance.connect();
                          // _scanForWTS();
                        },
                      )
                    : SizedBox())
                .toList(),
          ),
        )
      ],
    );
  }
}
