import 'package:flutter/material.dart';
import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'device_list.dart';
import 'reset_button.dart';
import '../whiztoys/whiztoys.dart';
import '../whiztoys/whiztoys_service.dart';
import '../whiztoys_view/led_state.dart';
import 'carpet_layout.dart';
import 'max_scale_text.dart';
import 'sensor_level_selector.dart';

class FindDeviceDialog extends StatefulWidget {
  const FindDeviceDialog({Key? key}) : super(key: key);

  @override
  State<FindDeviceDialog> createState() => _FindDeviceDialogState();
}

class _FindDeviceDialogState extends State<FindDeviceDialog> {
  @override
  void initState() {
    FlutterBluePlus.instance
        .startScan(timeout: const Duration(seconds: 4)); //一進頁面開始掃描
    super.initState();
  }

  @override
  void dispose() {
    FlutterBluePlus.instance.stopScan();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaxScaleTextWidget(
      max: 1.0,
      child: Material(
        color: Colors.transparent,
        child: Center(
          child: Container(
            margin: const EdgeInsets.all(
              24.0,
            ),
            padding: const EdgeInsets.all(8.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16.0),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0),
                      child: Text(
                        "拼接狀態",
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: const Icon(
                          Icons.cancel,
                          size: 32.0,
                          color: Color(0xffB6D9EF),
                        )),
                  ],
                ),
                const Divider(
                  thickness: 1.0,
                  color: Colors.black,
                ),
                Expanded(
                  child: OrientationBuilder(builder: (context, orientation) {
                    return Flex(
                      direction: orientation == Orientation.portrait
                          ? Axis.vertical
                          : Axis.horizontal,
                      children: const [
                        //左側
                        LeftSideWidgets(),
                        // 右側
                        RightSideWidgets(),
                      ],
                    );
                  }),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class LeftSideWidgets extends StatefulWidget {
  const LeftSideWidgets({Key? key}) : super(key: key);

  @override
  State<LeftSideWidgets> createState() => _LeftSideWidgetsState();
}

class _LeftSideWidgetsState extends State<LeftSideWidgets> {
  int level = 2;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder<BluetoothDeviceState>(
                  stream: WhizToysService.instance.deviceState,
                  builder: (context, bleDeviceState) {
                    if (bleDeviceState.data == null ||
                        bleDeviceState.data! !=
                            BluetoothDeviceState.connected) {
                      return const SizedBox();
                    }
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Expanded(
                          child: StreamBuilder<WhizToysLayout>(
                              stream: WhizToysService.instance.layout.stream,
                              builder: (context, snapshot) {
                                return WhizToysLayoutView(
                                  whizToysLayout: snapshot.data,
                                );
                              }),
                        ),
                        Column(
                          children: [
                            SensorLevelSelectorWidget(
                              level: level,
                              onChange: (SensorLevelSelector type) {
                                switch (type) {
                                  case SensorLevelSelector.add:
                                    if (level == 2) {
                                      level = 3;
                                    } else if (level == 1) {
                                      level = 2;
                                    }

                                    setState(() {});
                                    break;
                                  case SensorLevelSelector.delete:
                                    if (level == 3) {
                                      level = 2;
                                    } else {
                                      level = 1;
                                    }
                                    setState(() {});
                                    break;
                                }
                              },
                            ),
                            StreamBuilder<BluetoothDeviceState>(
                                stream: WhizToysService.instance.deviceState,
                                builder: (context, bleDeviceState) {
                                  return ResetButton(
                                    onTap: () {
                                      WhizToysService.instance
                                          .resetSensor(level);
                                      Fluttertoast.showToast(
                                          msg: "感測器校正中",
                                          toastLength: Toast.LENGTH_LONG,
                                          gravity: ToastGravity.CENTER,
                                          backgroundColor:
                                              Theme.of(context).primaryColor,
                                          textColor: Colors.white,
                                          fontSize: 16.0);
                                    },
                                  );
                                }),
                          ],
                        ),
                      ],
                    );
                  }),
            ),
            const Divider(
              thickness: 1,
            ),
            StreamBuilder<BluetoothDeviceState>(
                stream: WhizToysService.instance.deviceState,
                builder: (context, snapshot) {
                  String txt = "";
                  if (snapshot.data == null) {
                    return Text(txt);
                  }
                  switch (snapshot.data!) {
                    case BluetoothDeviceState.connected:
                      txt = "裝置已連接";
                      break;
                    case BluetoothDeviceState.connecting:
                      txt = "裝置連線中";
                      break;
                    case BluetoothDeviceState.disconnected:
                      txt = "裝置未連接";
                      break;
                    case BluetoothDeviceState.disconnecting:
                      txt = "裝置斷線中";
                      break;
                    default:
                  }
                  return Text(
                    txt,
                    style: const TextStyle(
                      fontSize: 24.0,
                      color: Color(0xff3686B2),
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }
}

class RightSideWidgets extends StatelessWidget {
  const RightSideWidgets({Key? key}) : super(key: key);

  Future _clearDevices() async {
    List<BluetoothDevice> devices =
        await FlutterBluePlus.instance.connectedDevices;
    await WhizToysService.instance.disconnect();
    devices.forEach((device) {
      if (device.name.contains('WTS')) {
        device.disconnect();
      }
    });
  }

  _scanForWTS() async {
    try {
      await FlutterBluePlus.instance
          .startScan(timeout: const Duration(seconds: 4));
    } catch (e) {
      //print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            const Expanded(
              child: DeviceListView(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                _buildButton(Color(0xFFEF6060), Color(0xFFEA5E5E), [
                  Text(
                    "斷開連線",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ], onTap: () {
                  _clearDevices();
                }),
                _buildButton(Color(0xFF64B3E0), Color(0xFF5D9CBF), [
                  Text(
                    "搜尋藍牙",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ], onTap: () {
                  _scanForWTS();
                }),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class DeviceTile extends StatelessWidget {
  const DeviceTile(
      {Key? key,
      this.title = "Device",
      this.subtitle = "",
      this.connected = false})
      : super(key: key);

  final String title, subtitle;
  final bool connected;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(8.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              border: Border.all(
                color: (connected) ? Color(0xff2e96f3) : Color(0x20000000),
                width: 3.0,
              )),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                  height: 40,
                  width: 40,
                  child: const Image(
                    image: AssetImage('assets/game_icons.png',
                        package: "whiztoys_ble"),
                  )),
              Text(
                title,
                style: Theme.of(context).textTheme.subtitle1,
              ),
              Text(
                "$subtitle",
                style: Theme.of(context).textTheme.subtitle1,
              ),
            ],
          ),
        ),
        Divider(),
      ],
    );
  }
}

//
class WhizToysLayoutView extends StatefulWidget {
  const WhizToysLayoutView({Key? key, this.whizToysLayout}) : super(key: key);
  final WhizToysLayout? whizToysLayout;
  @override
  State<WhizToysLayoutView> createState() => _WhizToysLayoutViewState();
}

class _WhizToysLayoutViewState extends State<WhizToysLayoutView> {
  late List<List<WhizPuzzle>> _whizPuzzles;

  @override
  void initState() {
    super.initState();
  }

  setLayout({WhizToysLayout? layout}) {
    List<List<WhizPuzzle>> _whizPuzzles;
    if (layout == null) {
      _whizPuzzles = List.generate(
        2,
        (row) => List.generate(
          3,
          (col) => WhizPuzzle(
              LedState(Colors.black, false, LedMode.none),
              LedState(Colors.black, false, LedMode.none),
              LedState(Colors.black, false, LedMode.none),
              LedState(Colors.black, false, LedMode.none),
              false,
              row,
              col,
              LedMode.none,
              Colors.black),
        ),
      );
    } else {
      int row = layout.sensorRow ~/ 2; //
      int col = layout.sensorCol ~/ 2;
      _whizPuzzles = List.generate(
        row,
        (rowIndex) => List.generate(
          col,
          (colIndex) => WhizPuzzle(
              LedState(Colors.black, false, LedMode.none),
              LedState(Colors.black, false, LedMode.none),
              LedState(Colors.black, false, LedMode.none),
              LedState(Colors.black, false, LedMode.none),
              false,
              rowIndex,
              colIndex,
              LedMode.none,
              Colors.black),
        ),
      );
      _whizPuzzles.forEach((element) {
        element.forEach((whizPuzzle) {
          // //print(
          //     "whizPuzzle.row= ${whizPuzzle.row}, whizPuzzle.col= ${whizPuzzle.col}");
          whizPuzzle.sensorValue = [
            layout.sensorLayoutArray[whizPuzzle.row * 2][whizPuzzle.col * 2],
            layout.sensorLayoutArray[whizPuzzle.row * 2 + 1]
                [whizPuzzle.col * 2],
            layout.sensorLayoutArray[whizPuzzle.row * 2 + 1]
                [whizPuzzle.col * 2 + 1],
            layout.sensorLayoutArray[whizPuzzle.row * 2]
                [whizPuzzle.col * 2 + 1],
          ];
          print(whizPuzzle.sensorValue);
        });
      });
    }
    this._whizPuzzles = _whizPuzzles;
  }

  @override
  Widget build(BuildContext context) {
    setLayout(layout: widget.whizToysLayout);

    return Container(
      child:
          //拼接狀態
          Align(
        alignment: Alignment.center,
        child: Container(
          width: MediaQuery.of(context).size.width * .5,
          height: MediaQuery.of(context).size.height * .6,
          child: LedCarpetViewWidget(
            whizPuzzles: _whizPuzzles,
            tapFunction: (row, col, index) {},
            showEar: false,
          ),
        ),
      ),
    );
  }
}

//
_buildButton(Color firstColor, Color secondColor, List<Widget> widgets,
    {required Function onTap}) {
  return Material(
    child: Ink(
      decoration: BoxDecoration(
        color: secondColor,
        borderRadius: BorderRadius.circular(5.0),
      ),
      padding: EdgeInsets.only(bottom: 2, left: 2, right: 2),
      child: InkWell(
        onTap: () => onTap(),
        child: Container(
          child: Ink(
            decoration: BoxDecoration(
              color: firstColor,
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: Container(
              width: 120.0,
              height: 40,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: widgets,
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
