import 'package:flutter/material.dart';
import '../whiztoys_view/led_state.dart';
import '../whiztoys_view/new_carpet_paint.dart';
import '../whiztoys_view/whiztoys_carpet_paint.dart';

class LedCarpetViewWidget extends StatelessWidget {
  const LedCarpetViewWidget(
      {Key? key,
      required this.whizPuzzles,
      required this.tapFunction,
      this.showEar = true})
      : super(key: key);
  final List<List<WhizPuzzle>> whizPuzzles;
  final Function(int, int, int) tapFunction;
  final showEar;

  @override
  Widget build(BuildContext context) {
    int row = whizPuzzles.length;
    int col = whizPuzzles[0].length;
    return LayoutBuilder(
      builder: (context, constrains) {
        final vCarpetSize = constrains.maxWidth / col;
        final hCarpetSize = constrains.maxHeight / row;

        dynamic carpetSize =
            vCarpetSize < hCarpetSize ? vCarpetSize : hCarpetSize;
        dynamic halfSize = carpetSize / 2;
        return Center(
          child: Container(
            width: carpetSize * col,
            height: carpetSize * row,
            child: GridView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: carpetSize, childAspectRatio: 1.0),
              itemCount: row * col,
              itemBuilder: (context, index) {
                if (whizPuzzles[index ~/ col][index % col].isEmpty) {
                  return SizedBox();
                }
                return GestureDetector(
                  onTapDown: (detail) {
                    int i = 0;
                    if (detail.localPosition.dx > halfSize &&
                        detail.localPosition.dy > halfSize) {
                      i = 2;
                    } else if (detail.localPosition.dx > halfSize) {
                      i = 3;
                    } else if (detail.localPosition.dy > halfSize) {
                      i = 1;
                    }
                    tapFunction(index ~/ col, index % col, i);
                  },
                  child: RepaintBoundary(
                    child: CustomPaint(
                      size: Size(50, (50 * 1).toDouble()),
                      painter: showEar
                          ? LedCarpetPainter(
                              whizPuzzles[index ~/ col][index % col],
                            )
                          : LedCarpetPainterNoEar(
                              whizPuzzles[index ~/ col][index % col],
                            ),
                    ),
                  ),
                );
              },
            ),
          ),
        );
      },
    );
  }
}
