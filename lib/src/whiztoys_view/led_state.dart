import 'package:flutter/material.dart';

import 'package:json_annotation/json_annotation.dart';

import '../whiztoys/whiztoys.dart';

part 'led_state.g.dart';

@JsonSerializable()
class LedState {
  LedState(this.ledColor, this.isLightOn, this.ledMode);
  LedMode ledMode;
  @JsonKey(fromJson: _colorFromJson, toJson: _colorToJson)
  Color ledColor;
  bool isLightOn;
  bool selected = false;
  late Paint _paint;
  get paint {
    if (ledColor == Colors.black) {
      _paint = Paint()
        ..style = PaintingStyle.stroke
        ..strokeWidth = 1;
      if (selected) {
        _paint.color = Colors.blue.withOpacity(1.0);
      } else {
        _paint.color = Colors.white.withOpacity(1.0);
      }
      return _paint;
    } else {
      _paint = Paint()
        ..style = PaintingStyle.fill
        ..strokeWidth = 1;
      _paint.color = ledColor.withOpacity(1.0);
    }
    return _paint;
  }

  LedState copy() {
    return LedState(
        Color(this.ledColor.value), isLightOn, LedMode.values[ledMode.index])
      ..selected = this.selected;
  }

  factory LedState.fromJson(Map<String, dynamic> json) =>
      _$LedStateFromJson(json);
  Map<String, dynamic> toJson() => _$LedStateToJson(this);
}

Color _colorFromJson(String colorString) {
  int intColor = int.parse(colorString, radix: 16);
  return new Color(intColor);
}

String _colorToJson(Color color) => color.value.toRadixString(16);

@JsonSerializable()
class WhizPuzzle {
  WhizPuzzle(this.leftTop, this.leftBottom, this.rightTop, this.rightBottom,
      this.selected, this.row, this.col, this.mode, this.ledColor) {
    this.ledColor = ledColor;
    leftTop.ledColor = ledColor;
    leftBottom.ledColor = ledColor;
    rightTop.ledColor = ledColor;
    rightBottom.ledColor = ledColor;
  }
  @JsonKey(name: 'lt')
  LedState leftTop;
  @JsonKey(name: 'lb')
  LedState leftBottom;
  @JsonKey(name: 'rt')
  LedState rightTop;
  @JsonKey(name: 'rb')
  LedState rightBottom;
  int row;
  int col;
  LedMode mode;

  @JsonKey(fromJson: _colorFromJson, toJson: _colorToJson)
  Color ledColor;

  bool selected, controlAllLed = true;

  late Paint _paint;

  @JsonKey(ignore: true)
  List<PressureStatus> sensorValue = [
    PressureStatus.NORMAL,
    PressureStatus.NORMAL,
    PressureStatus.NORMAL,
    PressureStatus.NORMAL
  ];
  bool get isEmpty => (sensorValue[0] == PressureStatus.EMPTY ||
      sensorValue[1] == PressureStatus.EMPTY ||
      sensorValue[2] == PressureStatus.EMPTY ||
      sensorValue[3] == PressureStatus.EMPTY);

  set specificLedColor(Color color) {
    if (controlAllLed && selected) this.ledColor = color;
    if (leftTop.selected) leftTop.ledColor = color;
    if (leftBottom.selected) leftBottom.ledColor = color;

    if (rightBottom.selected) rightBottom.ledColor = color;
    if (rightTop.selected) rightTop.ledColor = color;
  }

  set ledAllColor(Color color) {
    this.ledColor = color;
    leftTop.ledColor = color;
    leftBottom.ledColor = color;
    rightTop.ledColor = color;
    rightBottom.ledColor = color;
  }

  Paint get paint {
    if (selected) {
      _paint = Paint()
        ..style = PaintingStyle.stroke
        ..strokeWidth = 1;
      _paint.color = Colors.blue.withOpacity(1.0);
      _paint.strokeCap = StrokeCap.round;
      _paint.strokeJoin = StrokeJoin.round;
    } else {
      _paint = Paint()
        ..style = PaintingStyle.stroke
        ..strokeWidth = 1;
      _paint.color = Colors.black.withOpacity(1.0);
      _paint.strokeCap = StrokeCap.round;
      _paint.strokeJoin = StrokeJoin.round;
    }
    return _paint;
  }

  switchLed() {
    leftTop.isLightOn = !leftTop.isLightOn;
    leftBottom.isLightOn = !leftBottom.isLightOn;
    rightTop.isLightOn = !rightTop.isLightOn;
    rightBottom.isLightOn = !rightBottom.isLightOn;
  }

  selectCarpet(bool controlAllLed, index) {
    this.controlAllLed = controlAllLed;
    if (controlAllLed) {
      this.selected = !this.selected;
      this.leftTop.selected = this.selected;
      this.leftBottom.selected = this.selected;
      this.rightBottom.selected = this.selected;
      this.rightTop.selected = this.selected;
    } else {
      switch (index) {
        case 0:
          this.leftTop.selected = !this.leftTop.selected;
          break;
        case 1:
          this.leftBottom.selected = !this.leftBottom.selected;
          break;
        case 2:
          this.rightBottom.selected = !this.rightBottom.selected;
          break;
        case 3:
          this.rightTop.selected = !this.rightTop.selected;
          break;
      }
    }
  }

  void cancleSelected() {
    leftTop.selected = false;
    leftBottom.selected = false;
    rightBottom.selected = false;
    rightTop.selected = false;
    this.selected = false;
  }

  WhizPuzzle copyWith() {
    return WhizPuzzle(
        this.leftTop.copy(),
        this.leftBottom.copy(),
        this.rightTop.copy(),
        this.rightBottom.copy(),
        this.selected,
        this.row,
        this.col,
        LedMode.values[this.mode.index],
        Color(this.ledColor.value))
      ..controlAllLed = controlAllLed
      ..leftTop = this.leftTop.copy()
      ..leftBottom = this.leftBottom.copy()
      ..rightBottom = this.rightBottom.copy()
      ..rightTop = this.rightTop.copy();
  }

  factory WhizPuzzle.fromJson(Map<String, dynamic> json) =>
      _$WhizPuzzleFromJson(json);
  Map<String, dynamic> toJson() => _$WhizPuzzleToJson(this);

  @override
  String toString() {
    return "WhizPuzzle(hashcode:${this.hashCode} row:$row,col:$col,controlAll: $controlAllLed mode:$mode,color:$ledColor, selected:${this.selected} [${leftTop.selected},${leftBottom.selected},${rightBottom.selected}, ${rightTop.selected}]";
  }
}
