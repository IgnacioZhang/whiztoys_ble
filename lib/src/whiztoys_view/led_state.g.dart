// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'led_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LedState _$LedStateFromJson(Map<String, dynamic> json) => LedState(
      _colorFromJson(json['ledColor'] as String),
      json['isLightOn'] as bool,
      $enumDecode(_$LedModeEnumMap, json['ledMode']),
    )..selected = json['selected'] as bool;

Map<String, dynamic> _$LedStateToJson(LedState instance) => <String, dynamic>{
      'ledMode': _$LedModeEnumMap[instance.ledMode],
      'ledColor': _colorToJson(instance.ledColor),
      'isLightOn': instance.isLightOn,
      'selected': instance.selected,
    };

const _$LedModeEnumMap = {
  LedMode.notSetting: 'notSetting',
  LedMode.none: 'none',
  LedMode.basic: 'basic',
  LedMode.blink: 'blink',
  LedMode.breath: 'breath',
  LedMode.loopLed: 'loopLed',
  LedMode.colorful: 'colorful',
};

WhizPuzzle _$WhizPuzzleFromJson(Map<String, dynamic> json) => WhizPuzzle(
      LedState.fromJson(json['lt'] as Map<String, dynamic>),
      LedState.fromJson(json['lb'] as Map<String, dynamic>),
      LedState.fromJson(json['rt'] as Map<String, dynamic>),
      LedState.fromJson(json['rb'] as Map<String, dynamic>),
      json['selected'] as bool,
      json['row'] as int,
      json['col'] as int,
      $enumDecode(_$LedModeEnumMap, json['mode']),
      _colorFromJson(json['ledColor'] as String),
    )..controlAllLed = json['controlAllLed'] as bool;

Map<String, dynamic> _$WhizPuzzleToJson(WhizPuzzle instance) =>
    <String, dynamic>{
      'lt': instance.leftTop,
      'lb': instance.leftBottom,
      'rt': instance.rightTop,
      'rb': instance.rightBottom,
      'row': instance.row,
      'col': instance.col,
      'mode': _$LedModeEnumMap[instance.mode],
      'ledColor': _colorToJson(instance.ledColor),
      'selected': instance.selected,
      'controlAllLed': instance.controlAllLed,
    };
