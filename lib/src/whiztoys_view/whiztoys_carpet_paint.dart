import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import '../whiztoys/whiztoys.dart';
import 'led_state.dart';

class LedCarpetPainterNoEar extends CustomPainter {
  LedCarpetPainterNoEar(this._whizPuzzle)
      : leftTop = _whizPuzzle.leftTop,
        leftBottom = _whizPuzzle.leftBottom,
        rightBottom = _whizPuzzle.rightBottom,
        rightTop = _whizPuzzle.rightTop;
  WhizPuzzle _whizPuzzle;
  LedState leftTop;
  LedState leftBottom;
  LedState rightTop;
  LedState rightBottom;
  @override
  void paint(Canvas canvas, Size size) {
    print('paint');

    Paint paint = Paint();
    Path path = Path();
    Path ledPath1 = Path();
    Path ledPath2 = Path();

    // Path number 1 底色

    path = Path();
    path.moveTo(size.width * 0.04285714, 0);
    path.lineTo(size.width * 0.9571429, 0);
    path.arcToPoint(Offset(size.width, size.height * 0.04285714),
        radius: Radius.elliptical(
            size.width * 0.04285714, size.height * 0.04285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path.lineTo(size.width, size.height * 0.9571429);
    path.arcToPoint(Offset(size.width * 0.9571429, size.height),
        radius: Radius.elliptical(
            size.width * 0.04285714, size.height * 0.04285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path.lineTo(size.width * 0.04285714, size.height);
    path.arcToPoint(Offset(0, size.height * 0.9571429),
        radius: Radius.elliptical(
            size.width * 0.04285714, size.height * 0.04285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path.lineTo(0, size.height * 0.04285714);
    path.arcToPoint(Offset(size.width * 0.04285714, 0),
        radius: Radius.elliptical(
            size.width * 0.04285714, size.height * 0.04285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path.close();

    paint.color = Color(0xff444444).withOpacity(1.0);
    canvas.drawPath(path, paint);
    if (_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 2, size.height / 2),
          size.width / 8,
          paint
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);

    // Path number 2 左上之1

    ledPath1 = Path();
    ledPath1.moveTo(size.width / 3, size.height * 0.06);
    ledPath1.lineTo(size.width / 3, size.height * 0.06);
    ledPath1.cubicTo(size.width / 3, size.height * 0.06, size.width / 3,
        size.height * 0.06, size.width / 3, size.height * 0.06);
    ledPath1.cubicTo(size.width / 3, size.height * 0.06, size.width / 3,
        size.height * 0.06, size.width / 3, size.height * 0.06);
    ledPath1.cubicTo(size.width / 3, size.height * 0.06, size.width * 0.31,
        size.height * 0.08, size.width * 0.31, size.height * 0.08);
    ledPath1.cubicTo(size.width * 0.31, size.height * 0.08, size.width * 0.31,
        size.height * 0.08, size.width * 0.31, size.height * 0.08);
    ledPath1.cubicTo(size.width * 0.31, size.height * 0.08, size.width * 0.09,
        size.height * 0.08, size.width * 0.09, size.height * 0.08);
    ledPath1.cubicTo(size.width * 0.09, size.height * 0.08, size.width * 0.09,
        size.height * 0.08, size.width * 0.09, size.height * 0.08);
    ledPath1.cubicTo(size.width * 0.09, size.height * 0.08, size.width * 0.08,
        size.height * 0.06, size.width * 0.08, size.height * 0.06);
    ledPath1.cubicTo(size.width * 0.08, size.height * 0.06, size.width * 0.08,
        size.height * 0.06, size.width * 0.08, size.height * 0.06);
    ledPath1.cubicTo(size.width * 0.08, size.height * 0.06, size.width * 0.08,
        size.height * 0.06, size.width * 0.08, size.height * 0.06);
    ledPath1.cubicTo(size.width * 0.08, size.height * 0.06, size.width / 3,
        size.height * 0.06, size.width / 3, size.height * 0.06);
    canvas.drawPath(ledPath1, leftTop.paint);

    // Path number 3 左上之2
    ledPath2 = Path();
    ledPath2.moveTo(size.width * 0.06, size.height * 0.08);
    ledPath2.lineTo(size.width * 0.06, size.height * 0.08);
    ledPath2.cubicTo(size.width * 0.06, size.height * 0.08, size.width * 0.06,
        size.height * 0.08, size.width * 0.06, size.height * 0.08);
    ledPath2.cubicTo(size.width * 0.06, size.height * 0.08, size.width * 0.06,
        size.height * 0.08, size.width * 0.06, size.height * 0.08);
    ledPath2.cubicTo(size.width * 0.06, size.height * 0.08, size.width * 0.08,
        size.height * 0.09, size.width * 0.08, size.height * 0.09);
    ledPath2.cubicTo(size.width * 0.08, size.height * 0.09, size.width * 0.08,
        size.height * 0.09, size.width * 0.08, size.height * 0.09);
    ledPath2.cubicTo(size.width * 0.08, size.height * 0.09, size.width * 0.08,
        size.height * 0.31, size.width * 0.08, size.height * 0.31);
    ledPath2.cubicTo(size.width * 0.08, size.height * 0.31, size.width * 0.08,
        size.height * 0.31, size.width * 0.08, size.height * 0.31);
    ledPath2.cubicTo(size.width * 0.08, size.height * 0.31, size.width * 0.06,
        size.height / 3, size.width * 0.06, size.height / 3);
    ledPath2.cubicTo(size.width * 0.06, size.height / 3, size.width * 0.06,
        size.height / 3, size.width * 0.06, size.height / 3);
    ledPath2.cubicTo(size.width * 0.06, size.height / 3, size.width * 0.06,
        size.height / 3, size.width * 0.06, size.height / 3);
    ledPath2.cubicTo(size.width * 0.06, size.height / 3, size.width * 0.06,
        size.height * 0.08, size.width * 0.06, size.height * 0.08);

    canvas.drawPath(ledPath2, leftTop.paint);
    //選擇圓點
    if (leftTop.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4, size.height / 4),
          size.width / 8,
          paint
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[0].index > PressureStatus.NORMAL.index) {
      paint..style = ui.PaintingStyle.fill;
      paint..color = Colors.blue.withOpacity(0.5);
      // switch (_whizPuzzle.sensorValue[0]) {
      //   case PressureStatus.P1:
      //     paint_5_stroke..color = Colors.blue.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P2:
      //     paint_5_stroke..color = Colors.orange.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P3:
      //     paint_5_stroke..color = Colors.red.withOpacity(0.5);
      //     break;
      //   default:
      // }
      canvas.drawCircle(
          Offset(size.width / 4, size.height / 4), size.width / 8, paint);
    }

    // Path number 4 右上之1

    ledPath1 = Path();
    ledPath1.moveTo(size.width * 0.94, size.height / 3);
    ledPath1.lineTo(size.width * 0.94, size.height / 3);
    ledPath1.cubicTo(size.width * 0.94, size.height / 3, size.width * 0.94,
        size.height / 3, size.width * 0.94, size.height / 3);
    ledPath1.cubicTo(size.width * 0.94, size.height / 3, size.width * 0.94,
        size.height / 3, size.width * 0.94, size.height / 3);
    ledPath1.cubicTo(size.width * 0.94, size.height / 3, size.width * 0.92,
        size.height * 0.31, size.width * 0.92, size.height * 0.31);
    ledPath1.cubicTo(size.width * 0.92, size.height * 0.31, size.width * 0.92,
        size.height * 0.31, size.width * 0.92, size.height * 0.31);
    ledPath1.cubicTo(size.width * 0.92, size.height * 0.31, size.width * 0.92,
        size.height * 0.09, size.width * 0.92, size.height * 0.09);
    ledPath1.cubicTo(size.width * 0.92, size.height * 0.09, size.width * 0.92,
        size.height * 0.09, size.width * 0.92, size.height * 0.09);
    ledPath1.cubicTo(size.width * 0.92, size.height * 0.09, size.width * 0.94,
        size.height * 0.08, size.width * 0.94, size.height * 0.08);
    ledPath1.cubicTo(size.width * 0.94, size.height * 0.08, size.width * 0.94,
        size.height * 0.08, size.width * 0.94, size.height * 0.08);
    ledPath1.cubicTo(size.width * 0.94, size.height * 0.08, size.width * 0.94,
        size.height * 0.08, size.width * 0.94, size.height * 0.08);
    ledPath1.cubicTo(size.width * 0.94, size.height * 0.08, size.width * 0.94,
        size.height / 3, size.width * 0.94, size.height / 3);
    canvas.drawPath(ledPath1, rightTop.paint);

    // Path number 5 右上之2

    ledPath2 = Path();
    ledPath2.moveTo(size.width * 0.92, size.height * 0.06);
    ledPath2.lineTo(size.width * 0.92, size.height * 0.06);
    ledPath2.cubicTo(size.width * 0.92, size.height * 0.06, size.width * 0.92,
        size.height * 0.06, size.width * 0.92, size.height * 0.06);
    ledPath2.cubicTo(size.width * 0.92, size.height * 0.06, size.width * 0.92,
        size.height * 0.06, size.width * 0.92, size.height * 0.06);
    ledPath2.cubicTo(size.width * 0.92, size.height * 0.06, size.width * 0.91,
        size.height * 0.08, size.width * 0.91, size.height * 0.08);
    ledPath2.cubicTo(size.width * 0.91, size.height * 0.08, size.width * 0.91,
        size.height * 0.08, size.width * 0.91, size.height * 0.08);
    ledPath2.cubicTo(size.width * 0.91, size.height * 0.08, size.width * 0.69,
        size.height * 0.08, size.width * 0.69, size.height * 0.08);
    ledPath2.cubicTo(size.width * 0.69, size.height * 0.08, size.width * 0.69,
        size.height * 0.08, size.width * 0.69, size.height * 0.08);
    ledPath2.cubicTo(size.width * 0.69, size.height * 0.08, size.width * 0.67,
        size.height * 0.06, size.width * 0.67, size.height * 0.06);
    ledPath2.cubicTo(size.width * 0.67, size.height * 0.06, size.width * 0.67,
        size.height * 0.06, size.width * 0.67, size.height * 0.06);
    ledPath2.cubicTo(size.width * 0.67, size.height * 0.06, size.width * 0.67,
        size.height * 0.06, size.width * 0.67, size.height * 0.06);
    ledPath2.cubicTo(size.width * 0.67, size.height * 0.06, size.width * 0.92,
        size.height * 0.06, size.width * 0.92, size.height * 0.06);
    canvas.drawPath(ledPath2, rightTop.paint);

    //選擇圓點
    if (rightTop.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4 * 3, size.height / 4),
          size.width / 8,
          paint
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[3].index > PressureStatus.NORMAL.index) {
      paint..style = ui.PaintingStyle.fill;
      paint..color = Colors.blue.withOpacity(0.5);
      switch (_whizPuzzle.sensorValue[3]) {
        case PressureStatus.P1:
          paint..color = Colors.blue.withOpacity(0.5);
          break;
        case PressureStatus.P2:
          paint..color = Colors.orange.withOpacity(0.5);
          break;
        case PressureStatus.P3:
          paint..color = Colors.red.withOpacity(0.5);
          break;
        default:
      }
      canvas.drawCircle(
          Offset(size.width / 4 * 3, size.height / 4), size.width / 8, paint);
    }

    // Path number 6 右下之1

    paint.color = Color(0xffffffff).withOpacity(1);
    ledPath1 = Path();
    ledPath1.moveTo(size.width * 0.67, size.height * 0.94);
    ledPath1.lineTo(size.width * 0.67, size.height * 0.94);
    ledPath1.cubicTo(size.width * 0.67, size.height * 0.94, size.width * 0.67,
        size.height * 0.94, size.width * 0.67, size.height * 0.94);
    ledPath1.cubicTo(size.width * 0.67, size.height * 0.94, size.width * 0.67,
        size.height * 0.94, size.width * 0.67, size.height * 0.94);
    ledPath1.cubicTo(size.width * 0.67, size.height * 0.94, size.width * 0.68,
        size.height * 0.92, size.width * 0.68, size.height * 0.92);
    ledPath1.cubicTo(size.width * 0.69, size.height * 0.92, size.width * 0.69,
        size.height * 0.92, size.width * 0.69, size.height * 0.92);
    ledPath1.cubicTo(size.width * 0.69, size.height * 0.92, size.width * 0.91,
        size.height * 0.92, size.width * 0.91, size.height * 0.92);
    ledPath1.cubicTo(size.width * 0.91, size.height * 0.92, size.width * 0.91,
        size.height * 0.92, size.width * 0.91, size.height * 0.92);
    ledPath1.cubicTo(size.width * 0.91, size.height * 0.92, size.width * 0.92,
        size.height * 0.94, size.width * 0.92, size.height * 0.94);
    ledPath1.cubicTo(size.width * 0.92, size.height * 0.94, size.width * 0.92,
        size.height * 0.94, size.width * 0.92, size.height * 0.94);
    ledPath1.cubicTo(size.width * 0.92, size.height * 0.94, size.width * 0.92,
        size.height * 0.94, size.width * 0.92, size.height * 0.94);
    ledPath1.cubicTo(size.width * 0.92, size.height * 0.94, size.width * 0.67,
        size.height * 0.94, size.width * 0.67, size.height * 0.94);
    canvas.drawPath(ledPath1, rightBottom.paint);

    // Path number 7 右下之2

    ledPath2 = Path();
    ledPath2.moveTo(size.width * 0.94, size.height * 0.92);
    ledPath2.lineTo(size.width * 0.94, size.height * 0.92);
    ledPath2.cubicTo(size.width * 0.94, size.height * 0.92, size.width * 0.94,
        size.height * 0.92, size.width * 0.94, size.height * 0.92);
    ledPath2.cubicTo(size.width * 0.94, size.height * 0.92, size.width * 0.94,
        size.height * 0.92, size.width * 0.94, size.height * 0.92);
    ledPath2.cubicTo(size.width * 0.94, size.height * 0.92, size.width * 0.92,
        size.height * 0.91, size.width * 0.92, size.height * 0.91);
    ledPath2.cubicTo(size.width * 0.92, size.height * 0.91, size.width * 0.92,
        size.height * 0.91, size.width * 0.92, size.height * 0.91);
    ledPath2.cubicTo(size.width * 0.92, size.height * 0.91, size.width * 0.92,
        size.height * 0.69, size.width * 0.92, size.height * 0.69);
    ledPath2.cubicTo(size.width * 0.92, size.height * 0.69, size.width * 0.92,
        size.height * 0.69, size.width * 0.92, size.height * 0.69);
    ledPath2.cubicTo(size.width * 0.92, size.height * 0.69, size.width * 0.94,
        size.height * 0.67, size.width * 0.94, size.height * 0.67);
    ledPath2.cubicTo(size.width * 0.94, size.height * 0.67, size.width * 0.94,
        size.height * 0.67, size.width * 0.94, size.height * 0.67);
    ledPath2.cubicTo(size.width * 0.94, size.height * 0.67, size.width * 0.94,
        size.height * 0.67, size.width * 0.94, size.height * 0.67);
    ledPath2.cubicTo(size.width * 0.94, size.height * 0.67, size.width * 0.94,
        size.height * 0.92, size.width * 0.94, size.height * 0.92);
    canvas.drawPath(ledPath2, rightBottom.paint);

    //圓點
    if (rightBottom.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4 * 3, size.height / 4 * 3),
          size.width / 8,
          paint
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[2].index > PressureStatus.NORMAL.index) {
      paint..style = ui.PaintingStyle.fill;
      paint..color = Colors.blue.withOpacity(0.5);
      // switch (_whizPuzzle.sensorValue[2]) {
      //   case PressureStatus.P1:
      //     paint_3_stroke..color = Colors.blue.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P2:
      //     paint_3_stroke..color = Colors.orange.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P3:
      //     paint_3_stroke..color = Colors.red.withOpacity(0.5);
      //     break;
      //   default:
      // }
      canvas.drawCircle(Offset(size.width / 4 * 3, size.height / 4 * 3),
          size.width / 8, paint);
    }

    // Path number 8 左下之1

    ledPath1 = Path();
    ledPath1.moveTo(size.width * 0.06, size.height * 0.67);
    ledPath1.lineTo(size.width * 0.06, size.height * 0.67);
    ledPath1.cubicTo(size.width * 0.06, size.height * 0.67, size.width * 0.06,
        size.height * 0.67, size.width * 0.06, size.height * 0.67);
    ledPath1.cubicTo(size.width * 0.06, size.height * 0.67, size.width * 0.06,
        size.height * 0.67, size.width * 0.06, size.height * 0.67);
    ledPath1.cubicTo(size.width * 0.06, size.height * 0.67, size.width * 0.08,
        size.height * 0.69, size.width * 0.08, size.height * 0.69);
    ledPath1.cubicTo(size.width * 0.08, size.height * 0.69, size.width * 0.08,
        size.height * 0.69, size.width * 0.08, size.height * 0.69);
    ledPath1.cubicTo(size.width * 0.08, size.height * 0.69, size.width * 0.08,
        size.height * 0.91, size.width * 0.08, size.height * 0.91);
    ledPath1.cubicTo(size.width * 0.08, size.height * 0.91, size.width * 0.08,
        size.height * 0.91, size.width * 0.08, size.height * 0.91);
    ledPath1.cubicTo(size.width * 0.08, size.height * 0.91, size.width * 0.06,
        size.height * 0.92, size.width * 0.06, size.height * 0.92);
    ledPath1.cubicTo(size.width * 0.06, size.height * 0.92, size.width * 0.06,
        size.height * 0.92, size.width * 0.06, size.height * 0.92);
    ledPath1.cubicTo(size.width * 0.06, size.height * 0.92, size.width * 0.06,
        size.height * 0.92, size.width * 0.06, size.height * 0.92);
    ledPath1.cubicTo(size.width * 0.06, size.height * 0.92, size.width * 0.06,
        size.height * 0.67, size.width * 0.06, size.height * 0.67);
    canvas.drawPath(ledPath1, leftBottom.paint);

    // Path number 9 左下之2

    ledPath2 = Path();
    ledPath2.moveTo(size.width * 0.08, size.height * 0.94);
    ledPath2.lineTo(size.width * 0.08, size.height * 0.94);
    ledPath2.cubicTo(size.width * 0.08, size.height * 0.94, size.width * 0.08,
        size.height * 0.94, size.width * 0.08, size.height * 0.94);
    ledPath2.cubicTo(size.width * 0.08, size.height * 0.94, size.width * 0.08,
        size.height * 0.94, size.width * 0.08, size.height * 0.94);
    ledPath2.cubicTo(size.width * 0.08, size.height * 0.94, size.width * 0.09,
        size.height * 0.92, size.width * 0.09, size.height * 0.92);
    ledPath2.cubicTo(size.width * 0.09, size.height * 0.92, size.width * 0.09,
        size.height * 0.92, size.width * 0.09, size.height * 0.92);
    ledPath2.cubicTo(size.width * 0.09, size.height * 0.92, size.width * 0.31,
        size.height * 0.92, size.width * 0.31, size.height * 0.92);
    ledPath2.cubicTo(size.width * 0.31, size.height * 0.92, size.width * 0.31,
        size.height * 0.92, size.width * 0.31, size.height * 0.92);
    ledPath2.cubicTo(size.width * 0.31, size.height * 0.92, size.width / 3,
        size.height * 0.94, size.width / 3, size.height * 0.94);
    ledPath2.cubicTo(size.width / 3, size.height * 0.94, size.width / 3,
        size.height * 0.94, size.width / 3, size.height * 0.94);
    ledPath2.cubicTo(size.width / 3, size.height * 0.94, size.width / 3,
        size.height * 0.94, size.width / 3, size.height * 0.94);
    ledPath2.cubicTo(size.width / 3, size.height * 0.94, size.width * 0.08,
        size.height * 0.94, size.width * 0.08, size.height * 0.94);
    canvas.drawPath(ledPath2, leftBottom.paint);

    //選擇圓點
    if (leftBottom.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4, size.height / 4 * 3),
          size.width / 8,
          paint
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[1].index > PressureStatus.NORMAL.index) {
      paint..style = ui.PaintingStyle.fill;
      paint..color = Colors.blue.withOpacity(0.5);
      // switch (_whizPuzzle.sensorValue[1]) {
      //   case PressureStatus.P1:
      //     paint_1_stroke..color = Colors.blue.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P2:
      //     paint_1_stroke..color = Colors.orange.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P3:
      //     paint_1_stroke..color = Colors.red.withOpacity(0.5);
      //     break;
      //   default:
      // }
      canvas.drawCircle(
          Offset(size.width / 4, size.height / 4 * 3), size.width / 8, paint);
    }
    Path path_9 = Path();
    path_9.moveTo(size.width * 0.7625714, size.height * 0.3464286);
    path_9.arcToPoint(Offset(size.width * 0.7614286, size.height * 0.3428571),
        radius: Radius.elliptical(
            size.width * 0.009428571, size.height * 0.009428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7614286, size.height * 0.3397143),
        radius: Radius.elliptical(
            size.width * 0.01157143, size.height * 0.01157143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7625714, size.height * 0.3367143),
        radius: Radius.elliptical(
            size.width * 0.009428571, size.height * 0.009428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7677143, size.height * 0.3328571),
        radius: Radius.elliptical(
            size.width * 0.008285714, size.height * 0.008285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7710000, size.height * 0.3328571),
        radius: Radius.elliptical(
            size.width * 0.008571429, size.height * 0.008571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7740000, size.height * 0.3337143),
        radius: Radius.elliptical(
            size.width * 0.01042857, size.height * 0.01042857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7765714, size.height * 0.3358571),
        radius: Radius.elliptical(
            size.width * 0.009285714, size.height * 0.009285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7785714, size.height * 0.3387143),
        radius: Radius.elliptical(
            size.width * 0.03471429, size.height * 0.03471429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.lineTo(size.width * 0.7804286, size.height * 0.3420000);
    path_9.lineTo(size.width * 0.7825714, size.height * 0.3454286);
    path_9.cubicTo(
        size.width * 0.7825714,
        size.height * 0.3465714,
        size.width * 0.7840000,
        size.height * 0.3478571,
        size.width * 0.7847143,
        size.height * 0.3491429);
    path_9.lineTo(size.width * 0.7870000, size.height * 0.3532857);
    path_9.lineTo(size.width * 0.7894286, size.height * 0.3578571);
    path_9.lineTo(size.width * 0.7920000, size.height * 0.3627143);
    path_9.lineTo(size.width * 0.7945714, size.height * 0.3681429);
    path_9.cubicTo(
        size.width * 0.7955714,
        size.height * 0.3700000,
        size.width * 0.7964286,
        size.height * 0.3720000,
        size.width * 0.7974286,
        size.height * 0.3741429);
    path_9.lineTo(size.width * 0.8002857, size.height * 0.3805714);
    path_9.cubicTo(
        size.width * 0.8011429,
        size.height * 0.3828571,
        size.width * 0.8022857,
        size.height * 0.3852857,
        size.width * 0.8032857,
        size.height * 0.3877143);
    path_9.lineTo(size.width * 0.8062857, size.height * 0.3955714);
    path_9.quadraticBezierTo(size.width * 0.8078571, size.height * 0.3997143,
        size.width * 0.8092857, size.height * 0.4041429);
    path_9.cubicTo(
        size.width * 0.8102857,
        size.height * 0.4071429,
        size.width * 0.8114286,
        size.height * 0.4102857,
        size.width * 0.8124286,
        size.height * 0.4137143);
    path_9.cubicTo(
        size.width * 0.8134286,
        size.height * 0.4171429,
        size.width * 0.8144286,
        size.height * 0.4204286,
        size.width * 0.8154286,
        size.height * 0.4241429);
    path_9.cubicTo(
        size.width * 0.8164286,
        size.height * 0.4278571,
        size.width * 0.8172857,
        size.height * 0.4317143,
        size.width * 0.8182857,
        size.height * 0.4357143);
    path_9.cubicTo(
        size.width * 0.8192857,
        size.height * 0.4397143,
        size.width * 0.8200000,
        size.height * 0.4440000,
        size.width * 0.8208571,
        size.height * 0.4485714);
    path_9.cubicTo(
        size.width * 0.8217143,
        size.height * 0.4531429,
        size.width * 0.8225714,
        size.height * 0.4578571,
        size.width * 0.8231429,
        size.height * 0.4628571);
    path_9.cubicTo(
        size.width * 0.8237143,
        size.height * 0.4678571,
        size.width * 0.8244286,
        size.height * 0.4731429,
        size.width * 0.8250000,
        size.height * 0.4785714);
    path_9.cubicTo(
        size.width * 0.8255714,
        size.height * 0.4840000,
        size.width * 0.8258571,
        size.height * 0.4898571,
        size.width * 0.8261429,
        size.height * 0.4960000);
    path_9.cubicTo(
        size.width * 0.8264286,
        size.height * 0.5021429,
        size.width * 0.8261429,
        size.height * 0.5084286,
        size.width * 0.8261429,
        size.height * 0.5151429);
    path_9.cubicTo(
        size.width * 0.8261429,
        size.height * 0.5218571,
        size.width * 0.8261429,
        size.height * 0.5294286,
        size.width * 0.8250000,
        size.height * 0.5364286);
    path_9.cubicTo(
        size.width * 0.8238571,
        size.height * 0.5434286,
        size.width * 0.8232857,
        size.height * 0.5515714,
        size.width * 0.8220000,
        size.height * 0.5598571);
    path_9.cubicTo(
        size.width * 0.8207143,
        size.height * 0.5681429,
        size.width * 0.8188571,
        size.height * 0.5765714,
        size.width * 0.8165714,
        size.height * 0.5855714);
    path_9.cubicTo(
        size.width * 0.8142857,
        size.height * 0.5945714,
        size.width * 0.8114286,
        size.height * 0.6040000,
        size.width * 0.8080000,
        size.height * 0.6141429);
    path_9.arcToPoint(Offset(size.width * 0.7952857, size.height * 0.6450000),
        radius:
            Radius.elliptical(size.width * 0.2915714, size.height * 0.2915714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7771429, size.height * 0.6778571),
        radius:
            Radius.elliptical(size.width * 0.3491429, size.height * 0.3491429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7750000, size.height * 0.6802857),
        radius: Radius.elliptical(
            size.width * 0.007000000, size.height * 0.007000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7721429, size.height * 0.6817143),
        radius: Radius.elliptical(
            size.width * 0.009000000, size.height * 0.009000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7690000, size.height * 0.6817143),
        radius: Radius.elliptical(
            size.width * 0.007857143, size.height * 0.007857143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7658571, size.height * 0.6808571),
        radius: Radius.elliptical(
            size.width * 0.007857143, size.height * 0.007857143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7634286, size.height * 0.6787143),
        radius: Radius.elliptical(
            size.width * 0.007142857, size.height * 0.007142857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7617143, size.height * 0.6758571),
        radius: Radius.elliptical(
            size.width * 0.008000000, size.height * 0.008000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7617143, size.height * 0.6725714),
        radius: Radius.elliptical(
            size.width * 0.01214286, size.height * 0.01214286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7627143, size.height * 0.6692857),
        radius: Radius.elliptical(
            size.width * 0.008857143, size.height * 0.008857143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7645714, size.height * 0.6661429),
        radius: Radius.elliptical(
            size.width * 0.01428571, size.height * 0.01428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.7665714, size.height * 0.6628571),
        radius: Radius.elliptical(
            size.width * 0.02514286, size.height * 0.02514286),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_9.cubicTo(
        size.width * 0.7665714,
        size.height * 0.6617143,
        size.width * 0.7680000,
        size.height * 0.6604286,
        size.width * 0.7687143,
        size.height * 0.6591429);
    path_9.lineTo(size.width * 0.7708571, size.height * 0.6552857);
    path_9.cubicTo(
        size.width * 0.7717143,
        size.height * 0.6540000,
        size.width * 0.7724286,
        size.height * 0.6524286,
        size.width * 0.7732857,
        size.height * 0.6510000);
    path_9.cubicTo(
        size.width * 0.7741429,
        size.height * 0.6495714,
        size.width * 0.7748571,
        size.height * 0.6480000,
        size.width * 0.7757143,
        size.height * 0.6462857);
    path_9.lineTo(size.width * 0.7782857, size.height * 0.6411429);
    path_9.lineTo(size.width * 0.7810000, size.height * 0.6355714);
    path_9.cubicTo(
        size.width * 0.7820000,
        size.height * 0.6335714,
        size.width * 0.7828571,
        size.height * 0.6314286,
        size.width * 0.7838571,
        size.height * 0.6292857);
    path_9.cubicTo(
        size.width * 0.7848571,
        size.height * 0.6271429,
        size.width * 0.7858571,
        size.height * 0.6248571,
        size.width * 0.7867143,
        size.height * 0.6225714);
    path_9.cubicTo(
        size.width * 0.7875714,
        size.height * 0.6202857,
        size.width * 0.7887143,
        size.height * 0.6177143,
        size.width * 0.7897143,
        size.height * 0.6150000);
    path_9.lineTo(size.width * 0.7927143, size.height * 0.6068571);
    path_9.cubicTo(
        size.width * 0.7937143,
        size.height * 0.6040000,
        size.width * 0.7947143,
        size.height * 0.6010000,
        size.width * 0.7957143,
        size.height * 0.5977143);
    path_9.cubicTo(
        size.width * 0.7967143,
        size.height * 0.5944286,
        size.width * 0.7977143,
        size.height * 0.5912857,
        size.width * 0.7985714,
        size.height * 0.5877143);
    path_9.cubicTo(
        size.width * 0.7994286,
        size.height * 0.5841429,
        size.width * 0.8005714,
        size.height * 0.5805714,
        size.width * 0.8014286,
        size.height * 0.5767143);
    path_9.cubicTo(
        size.width * 0.8022857,
        size.height * 0.5728571,
        size.width * 0.8032857,
        size.height * 0.5687143,
        size.width * 0.8040000,
        size.height * 0.5644286);
    path_9.cubicTo(
        size.width * 0.8047143,
        size.height * 0.5601429,
        size.width * 0.8057143,
        size.height * 0.5557143,
        size.width * 0.8062857,
        size.height * 0.5510000);
    path_9.cubicTo(
        size.width * 0.8068571,
        size.height * 0.5462857,
        size.width * 0.8075714,
        size.height * 0.5412857,
        size.width * 0.8081429,
        size.height * 0.5367143);
    path_9.cubicTo(
        size.width * 0.8087143,
        size.height * 0.5321429,
        size.width * 0.8090000,
        size.height * 0.5260000,
        size.width * 0.8092857,
        size.height * 0.5201429);
    path_9.cubicTo(
        size.width * 0.8095714,
        size.height * 0.5142857,
        size.width * 0.8092857,
        size.height * 0.5082857,
        size.width * 0.8092857,
        size.height * 0.5018571);
    path_9.cubicTo(
        size.width * 0.8092857,
        size.height * 0.4954286,
        size.width * 0.8092857,
        size.height * 0.4887143,
        size.width * 0.8081429,
        size.height * 0.4815714);
    path_9.cubicTo(
        size.width * 0.8070000,
        size.height * 0.4744286,
        size.width * 0.8065714,
        size.height * 0.4672857,
        size.width * 0.8052857,
        size.height * 0.4592857);
    path_9.cubicTo(
        size.width * 0.8040000,
        size.height * 0.4512857,
        size.width * 0.8021429,
        size.height * 0.4428571,
        size.width * 0.8000000,
        size.height * 0.4340000);
    path_9.cubicTo(
        size.width * 0.7978571,
        size.height * 0.4251429,
        size.width * 0.7951429,
        size.height * 0.4164286,
        size.width * 0.7918571,
        size.height * 0.4071429);
    path_9.arcToPoint(Offset(size.width * 0.7797143, size.height * 0.3777143),
        radius:
            Radius.elliptical(size.width * 0.3107143, size.height * 0.3107143),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_9.arcToPoint(Offset(size.width * 0.7625714, size.height * 0.3464286),
        radius:
            Radius.elliptical(size.width * 0.3324286, size.height * 0.3324286),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_9.close();

    Paint paint_9_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.003571429;
    paint_9_stroke.color = Color(0xff000000).withOpacity(1.0);
    paint_9_stroke.strokeCap = StrokeCap.round;
    paint_9_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_9, paint_9_stroke);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.3448571, size.height * 0.7627143);
    path_10.cubicTo(
        size.width * 0.3545714,
        size.height * 0.7685714,
        size.width * 0.3642857,
        size.height * 0.7741429,
        size.width * 0.3744286,
        size.height * 0.7790000);
    path_10.cubicTo(
        size.width * 0.3845714,
        size.height * 0.7838571,
        size.width * 0.3925714,
        size.height * 0.7871429,
        size.width * 0.4018571,
        size.height * 0.7905714);
    path_10.cubicTo(
        size.width * 0.4111429,
        size.height * 0.7940000,
        size.width * 0.4185714,
        size.height * 0.7962857,
        size.width * 0.4271429,
        size.height * 0.7985714);
    path_10.cubicTo(
        size.width * 0.4357143,
        size.height * 0.8008571,
        size.width * 0.4427143,
        size.height * 0.8024286,
        size.width * 0.4505714,
        size.height * 0.8040000);
    path_10.cubicTo(
        size.width * 0.4584286,
        size.height * 0.8055714,
        size.width * 0.4648571,
        size.height * 0.8062857,
        size.width * 0.4720000,
        size.height * 0.8071429);
    path_10.cubicTo(
        size.width * 0.4791429,
        size.height * 0.8080000,
        size.width * 0.4848571,
        size.height * 0.8084286,
        size.width * 0.4912857,
        size.height * 0.8087143);
    path_10.cubicTo(
        size.width * 0.4977143,
        size.height * 0.8090000,
        size.width * 0.5030000,
        size.height * 0.8087143,
        size.width * 0.5088571,
        size.height * 0.8087143);
    path_10.cubicTo(
        size.width * 0.5147143,
        size.height * 0.8087143,
        size.width * 0.5195714,
        size.height * 0.8087143,
        size.width * 0.5248571,
        size.height * 0.8087143);
    path_10.lineTo(size.width * 0.5391429, size.height * 0.8075714);
    path_10.lineTo(size.width * 0.5521429, size.height * 0.8057143);
    path_10.lineTo(size.width * 0.5638571, size.height * 0.8037143);
    path_10.lineTo(size.width * 0.5747143, size.height * 0.8014286);
    path_10.lineTo(size.width * 0.5847143, size.height * 0.7988571);
    path_10.lineTo(size.width * 0.5937143, size.height * 0.7962857);
    path_10.lineTo(size.width * 0.6020000, size.height * 0.7937143);
    path_10.lineTo(size.width * 0.6092857, size.height * 0.7911429);
    path_10.lineTo(size.width * 0.6158571, size.height * 0.7887143);
    path_10.lineTo(size.width * 0.6218571, size.height * 0.7862857);
    path_10.lineTo(size.width * 0.6272857, size.height * 0.7840000);
    path_10.lineTo(size.width * 0.6321429, size.height * 0.7818571);
    path_10.lineTo(size.width * 0.6364286, size.height * 0.7797143);
    path_10.lineTo(size.width * 0.6404286, size.height * 0.7777143);
    path_10.lineTo(size.width * 0.6441429, size.height * 0.7758571);
    path_10.lineTo(size.width * 0.6475714, size.height * 0.7741429);
    path_10.lineTo(size.width * 0.6508571, size.height * 0.7724286);
    path_10.lineTo(size.width * 0.6538571, size.height * 0.7707143);
    path_10.lineTo(size.width * 0.6568571, size.height * 0.7690000);
    path_10.lineTo(size.width * 0.6597143, size.height * 0.7672857);
    path_10.lineTo(size.width * 0.6625714, size.height * 0.7655714);
    path_10.lineTo(size.width * 0.6655714, size.height * 0.7637143);
    path_10.lineTo(size.width * 0.6684286, size.height * 0.7621429);
    path_10.arcToPoint(Offset(size.width * 0.6715714, size.height * 0.7621429),
        radius: Radius.elliptical(
            size.width * 0.007714286, size.height * 0.007714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6744286, size.height * 0.7621429),
        radius: Radius.elliptical(
            size.width * 0.01014286, size.height * 0.01014286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6768571, size.height * 0.7634286),
        radius: Radius.elliptical(
            size.width * 0.008000000, size.height * 0.008000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6788571, size.height * 0.7657143),
        radius: Radius.elliptical(
            size.width * 0.01528571, size.height * 0.01528571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6800000, size.height * 0.7684286),
        radius: Radius.elliptical(
            size.width * 0.01542857, size.height * 0.01542857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6800000, size.height * 0.7737143),
        radius: Radius.elliptical(
            size.width * 0.009714286, size.height * 0.009714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6785714, size.height * 0.7758571),
        radius: Radius.elliptical(
            size.width * 0.008000000, size.height * 0.008000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6765714, size.height * 0.7775714),
        radius: Radius.elliptical(
            size.width * 0.007571429, size.height * 0.007571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6467143, size.height * 0.7941429),
        radius:
            Radius.elliptical(size.width * 0.3377143, size.height * 0.3377143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6192857, size.height * 0.8060000),
        radius:
            Radius.elliptical(size.width * 0.2888571, size.height * 0.2888571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.cubicTo(
        size.width * 0.6108571,
        size.height * 0.8091429,
        size.width * 0.6022857,
        size.height * 0.8120000,
        size.width * 0.5937143,
        size.height * 0.8144286);
    path_10.cubicTo(
        size.width * 0.5851429,
        size.height * 0.8168571,
        size.width * 0.5781429,
        size.height * 0.8185714,
        size.width * 0.5701429,
        size.height * 0.8201429);
    path_10.cubicTo(
        size.width * 0.5621429,
        size.height * 0.8217143,
        size.width * 0.5558571,
        size.height * 0.8228571,
        size.width * 0.5488571,
        size.height * 0.8237143);
    path_10.cubicTo(
        size.width * 0.5418571,
        size.height * 0.8245714,
        size.width * 0.5361429,
        size.height * 0.8252857,
        size.width * 0.5297143,
        size.height * 0.8257143);
    path_10.cubicTo(
        size.width * 0.5232857,
        size.height * 0.8261429,
        size.width * 0.5181429,
        size.height * 0.8257143,
        size.width * 0.5122857,
        size.height * 0.8265714);
    path_10.cubicTo(
        size.width * 0.5064286,
        size.height * 0.8274286,
        size.width * 0.5017143,
        size.height * 0.8265714,
        size.width * 0.4965714,
        size.height * 0.8265714);
    path_10.lineTo(size.width * 0.4822857, size.height * 0.8265714);
    path_10.lineTo(size.width * 0.4680000, size.height * 0.8251429);
    path_10.lineTo(size.width * 0.4555714, size.height * 0.8234286);
    path_10.lineTo(size.width * 0.4441429, size.height * 0.8214286);
    path_10.lineTo(size.width * 0.4337143, size.height * 0.8192857);
    path_10.lineTo(size.width * 0.4242857, size.height * 0.8168571);
    path_10.lineTo(size.width * 0.4157143, size.height * 0.8144286);
    path_10.lineTo(size.width * 0.4080000, size.height * 0.8121429);
    path_10.lineTo(size.width * 0.4010000, size.height * 0.8097143);
    path_10.lineTo(size.width * 0.3947143, size.height * 0.8075714);
    path_10.lineTo(size.width * 0.3890000, size.height * 0.8052857);
    path_10.lineTo(size.width * 0.3838571, size.height * 0.8032857);
    path_10.lineTo(size.width * 0.3792857, size.height * 0.8000000);
    path_10.lineTo(size.width * 0.3750000, size.height * 0.7982857);
    path_10.lineTo(size.width * 0.3714286, size.height * 0.7967143);
    path_10.lineTo(size.width * 0.3680000, size.height * 0.7950000);
    path_10.lineTo(size.width * 0.3648571, size.height * 0.7935714);
    path_10.lineTo(size.width * 0.3620000, size.height * 0.7921429);
    path_10.lineTo(size.width * 0.3592857, size.height * 0.7907143);
    path_10.lineTo(size.width * 0.3568571, size.height * 0.7892857);
    path_10.lineTo(size.width * 0.3544286, size.height * 0.7880000);
    path_10.lineTo(size.width * 0.3520000, size.height * 0.7868571);
    path_10.lineTo(size.width * 0.3495714, size.height * 0.7857143);
    path_10.lineTo(size.width * 0.3472857, size.height * 0.7842857);
    path_10.lineTo(size.width * 0.3448571, size.height * 0.7828571);
    path_10.lineTo(size.width * 0.3422857, size.height * 0.7814286);
    path_10.lineTo(size.width * 0.3397143, size.height * 0.7798571);
    path_10.lineTo(size.width * 0.3368571, size.height * 0.7781429);
    path_10.arcToPoint(Offset(size.width * 0.3342857, size.height * 0.7762857),
        radius: Radius.elliptical(
            size.width * 0.01185714, size.height * 0.01185714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3324286, size.height * 0.7737143),
        radius: Radius.elliptical(
            size.width * 0.006571429, size.height * 0.006571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3348571, size.height * 0.7632857),
        radius: Radius.elliptical(
            size.width * 0.008428571, size.height * 0.008428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3372857, size.height * 0.7620000),
        radius: Radius.elliptical(
            size.width * 0.006571429, size.height * 0.006571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3398571, size.height * 0.7620000),
        radius: Radius.elliptical(
            size.width * 0.005285714, size.height * 0.005285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3424286, size.height * 0.7620000),
        radius: Radius.elliptical(
            size.width * 0.005285714, size.height * 0.005285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3448571, size.height * 0.7627143),
        radius: Radius.elliptical(
            size.width * 0.005428571, size.height * 0.005428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.close();

    Paint paint_10_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.003571429;
    paint_10_stroke.color = Color(0xff000000).withOpacity(1.0);
    paint_10_stroke.strokeCap = StrokeCap.round;
    paint_10_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_10, paint_10_stroke);

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.3448571, size.height * 0.2494286);
    path_11.lineTo(size.width * 0.3425714, size.height * 0.2504286);
    path_11.arcToPoint(Offset(size.width * 0.3372857, size.height * 0.2504286),
        radius: Radius.elliptical(
            size.width * 0.008285714, size.height * 0.008285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.3348571, size.height * 0.2491429),
        radius: Radius.elliptical(
            size.width * 0.008000000, size.height * 0.008000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.3328571, size.height * 0.2470000),
        radius: Radius.elliptical(
            size.width * 0.01428571, size.height * 0.01428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.3318571, size.height * 0.2441429),
        radius: Radius.elliptical(
            size.width * 0.01142857, size.height * 0.01142857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.3318571, size.height * 0.2385714),
        radius: Radius.elliptical(
            size.width * 0.009285714, size.height * 0.009285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.3337143, size.height * 0.2361429),
        radius: Radius.elliptical(
            size.width * 0.01000000, size.height * 0.01000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.3362857, size.height * 0.2342857),
        radius: Radius.elliptical(
            size.width * 0.01428571, size.height * 0.01428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.lineTo(size.width * 0.3391429, size.height * 0.2324286);
    path_11.lineTo(size.width * 0.3418571, size.height * 0.2308571);
    path_11.lineTo(size.width * 0.3442857, size.height * 0.2294286);
    path_11.lineTo(size.width * 0.3467143, size.height * 0.2280000);
    path_11.lineTo(size.width * 0.3491429, size.height * 0.2267143);
    path_11.lineTo(size.width * 0.3514286, size.height * 0.2254286);
    path_11.lineTo(size.width * 0.3537143, size.height * 0.2241429);
    path_11.lineTo(size.width * 0.3561429, size.height * 0.2228571);
    path_11.lineTo(size.width * 0.3587143, size.height * 0.2215714);
    path_11.lineTo(size.width * 0.3614286, size.height * 0.2201429);
    path_11.lineTo(size.width * 0.3642857, size.height * 0.2187143);
    path_11.lineTo(size.width * 0.3674286, size.height * 0.2171429);
    path_11.lineTo(size.width * 0.3707143, size.height * 0.2155714);
    path_11.lineTo(size.width * 0.3745714, size.height * 0.2138571);
    path_11.lineTo(size.width * 0.3787143, size.height * 0.2120000);
    path_11.lineTo(size.width * 0.3832857, size.height * 0.2101429);
    path_11.lineTo(size.width * 0.3884286, size.height * 0.2080000);
    path_11.lineTo(size.width * 0.3941429, size.height * 0.2058571);
    path_11.lineTo(size.width * 0.4004286, size.height * 0.2035714);
    path_11.lineTo(size.width * 0.4074286, size.height * 0.2012857);
    path_11.lineTo(size.width * 0.4151429, size.height * 0.1988571);
    path_11.lineTo(size.width * 0.4237143, size.height * 0.1964286);
    path_11.lineTo(size.width * 0.4332857, size.height * 0.1941429);
    path_11.lineTo(size.width * 0.4435714, size.height * 0.1918571);
    path_11.lineTo(size.width * 0.4550000, size.height * 0.1898571);
    path_11.lineTo(size.width * 0.4674286, size.height * 0.1881429);
    path_11.lineTo(size.width * 0.4817143, size.height * 0.1868571);
    path_11.lineTo(size.width * 0.4960000, size.height * 0.1860000);
    path_11.lineTo(size.width * 0.5117143, size.height * 0.1860000);
    path_11.cubicTo(
        size.width * 0.5175714,
        size.height * 0.1860000,
        size.width * 0.5234286,
        size.height * 0.1860000,
        size.width * 0.5291429,
        size.height * 0.1867143);
    path_11.cubicTo(
        size.width * 0.5348571,
        size.height * 0.1874286,
        size.width * 0.5420000,
        size.height * 0.1878571,
        size.width * 0.5484286,
        size.height * 0.1887143);
    path_11.cubicTo(
        size.width * 0.5548571,
        size.height * 0.1895714,
        size.width * 0.5627143,
        size.height * 0.1908571,
        size.width * 0.5697143,
        size.height * 0.1922857);
    path_11.cubicTo(
        size.width * 0.5767143,
        size.height * 0.1937143,
        size.width * 0.5854286,
        size.height * 0.1958571,
        size.width * 0.5931429,
        size.height * 0.1980000);
    path_11.cubicTo(
        size.width * 0.6008571,
        size.height * 0.2001429,
        size.width * 0.6102857,
        size.height * 0.2032857,
        size.width * 0.6187143,
        size.height * 0.2064286);
    path_11.cubicTo(
        size.width * 0.6271429,
        size.height * 0.2095714,
        size.width * 0.6372857,
        size.height * 0.2138571,
        size.width * 0.6461429,
        size.height * 0.2182857);
    path_11.cubicTo(
        size.width * 0.6550000,
        size.height * 0.2227143,
        size.width * 0.6664286,
        size.height * 0.2290000,
        size.width * 0.6761429,
        size.height * 0.2350000);
    path_11.arcToPoint(Offset(size.width * 0.6780000, size.height * 0.2365714),
        radius: Radius.elliptical(
            size.width * 0.009857143, size.height * 0.009857143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.6794286, size.height * 0.2388571),
        radius: Radius.elliptical(
            size.width * 0.007714286, size.height * 0.007714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.6794286, size.height * 0.2441429),
        radius: Radius.elliptical(
            size.width * 0.008285714, size.height * 0.008285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.6782857, size.height * 0.2468571),
        radius: Radius.elliptical(
            size.width * 0.009142857, size.height * 0.009142857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.6711429, size.height * 0.2508571),
        radius: Radius.elliptical(
            size.width * 0.008714286, size.height * 0.008714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.6680000, size.height * 0.2508571),
        radius: Radius.elliptical(
            size.width * 0.007714286, size.height * 0.007714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.6650000, size.height * 0.2492857),
        radius: Radius.elliptical(
            size.width * 0.01514286, size.height * 0.01514286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.lineTo(size.width * 0.6621429, size.height * 0.2474286);
    path_11.lineTo(size.width * 0.6592857, size.height * 0.2457143);
    path_11.lineTo(size.width * 0.6571429, size.height * 0.2428571);
    path_11.lineTo(size.width * 0.6541429, size.height * 0.2412857);
    path_11.lineTo(size.width * 0.6510000, size.height * 0.2395714);
    path_11.lineTo(size.width * 0.6478571, size.height * 0.2378571);
    path_11.lineTo(size.width * 0.6444286, size.height * 0.2360000);
    path_11.lineTo(size.width * 0.6407143, size.height * 0.2341429);
    path_11.lineTo(size.width * 0.6367143, size.height * 0.2321429);
    path_11.lineTo(size.width * 0.6322857, size.height * 0.2301429);
    path_11.lineTo(size.width * 0.6275714, size.height * 0.2285714);
    path_11.lineTo(size.width * 0.6222857, size.height * 0.2262857);
    path_11.lineTo(size.width * 0.6162857, size.height * 0.2238571);
    path_11.lineTo(size.width * 0.6097143, size.height * 0.2214286);
    path_11.arcToPoint(Offset(size.width * 0.6022857, size.height * 0.2188571),
        radius: Radius.elliptical(
            size.width * 0.07628571, size.height * 0.07628571),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_11.lineTo(size.width * 0.5941429, size.height * 0.2161429);
    path_11.lineTo(size.width * 0.5857143, size.height * 0.2132857);
    path_11.lineTo(size.width * 0.5757143, size.height * 0.2108571);
    path_11.lineTo(size.width * 0.5648571, size.height * 0.2084286);
    path_11.lineTo(size.width * 0.5530000, size.height * 0.2064286);
    path_11.cubicTo(
        size.width * 0.5487143,
        size.height * 0.2064286,
        size.width * 0.5444286,
        size.height * 0.2051429,
        size.width * 0.5400000,
        size.height * 0.2047143);
    path_11.cubicTo(
        size.width * 0.5355714,
        size.height * 0.2042857,
        size.width * 0.5304286,
        size.height * 0.2037143,
        size.width * 0.5257143,
        size.height * 0.2034286);
    path_11.lineTo(size.width * 0.5097143, size.height * 0.2034286);
    path_11.lineTo(size.width * 0.4921429, size.height * 0.2034286);
    path_11.cubicTo(
        size.width * 0.4855714,
        size.height * 0.2034286,
        size.width * 0.4791429,
        size.height * 0.2034286,
        size.width * 0.4727143,
        size.height * 0.2050000);
    path_11.cubicTo(
        size.width * 0.4662857,
        size.height * 0.2065714,
        size.width * 0.4584286,
        size.height * 0.2068571,
        size.width * 0.4512857,
        size.height * 0.2081429);
    path_11.cubicTo(
        size.width * 0.4441429,
        size.height * 0.2094286,
        size.width * 0.4357143,
        size.height * 0.2114286,
        size.width * 0.4280000,
        size.height * 0.2134286);
    path_11.cubicTo(
        size.width * 0.4202857,
        size.height * 0.2154286,
        size.width * 0.4110000,
        size.height * 0.2184286,
        size.width * 0.4025714,
        size.height * 0.2215714);
    path_11.arcToPoint(Offset(size.width * 0.3752857, size.height * 0.2331429),
        radius:
            Radius.elliptical(size.width * 0.2402857, size.height * 0.2402857),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_11.cubicTo(
        size.width * 0.3642857,
        size.height * 0.2380000,
        size.width * 0.3545714,
        size.height * 0.2435714,
        size.width * 0.3448571,
        size.height * 0.2494286);
    path_11.close();

    Paint paint_11_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.003571429;
    paint_11_stroke.color = Color(0xff000000).withOpacity(1.0);
    paint_11_stroke.strokeCap = StrokeCap.round;
    paint_11_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_11, paint_11_stroke);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.2494286, size.height * 0.3464286);
    path_12.arcToPoint(Offset(size.width * 0.2322857, size.height * 0.3778571),
        radius:
            Radius.elliptical(size.width * 0.3348571, size.height * 0.3348571),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_12.arcToPoint(Offset(size.width * 0.2201429, size.height * 0.4071429),
        radius:
            Radius.elliptical(size.width * 0.3081429, size.height * 0.3081429),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_12.cubicTo(
        size.width * 0.2168571,
        size.height * 0.4165714,
        size.width * 0.2142857,
        size.height * 0.4255714,
        size.width * 0.2121429,
        size.height * 0.4341429);
    path_12.cubicTo(
        size.width * 0.2100000,
        size.height * 0.4427143,
        size.width * 0.2082857,
        size.height * 0.4508571,
        size.width * 0.2070000,
        size.height * 0.4587143);
    path_12.cubicTo(
        size.width * 0.2057143,
        size.height * 0.4665714,
        size.width * 0.2048571,
        size.height * 0.4740000,
        size.width * 0.2041429,
        size.height * 0.4810000);
    path_12.cubicTo(
        size.width * 0.2034286,
        size.height * 0.4880000,
        size.width * 0.2031429,
        size.height * 0.4952857,
        size.width * 0.2030000,
        size.height * 0.5012857);
    path_12.cubicTo(
        size.width * 0.2028571,
        size.height * 0.5072857,
        size.width * 0.2030000,
        size.height * 0.5138571,
        size.width * 0.2030000,
        size.height * 0.5197143);
    path_12.cubicTo(
        size.width * 0.2030000,
        size.height * 0.5255714,
        size.width * 0.2030000,
        size.height * 0.5310000,
        size.width * 0.2040000,
        size.height * 0.5361429);
    path_12.cubicTo(
        size.width * 0.2050000,
        size.height * 0.5412857,
        size.width * 0.2051429,
        size.height * 0.5464286,
        size.width * 0.2058571,
        size.height * 0.5504286);
    path_12.cubicTo(
        size.width * 0.2065714,
        size.height * 0.5544286,
        size.width * 0.2072857,
        size.height * 0.5597143,
        size.width * 0.2081429,
        size.height * 0.5647143);
    path_12.cubicTo(
        size.width * 0.2090000,
        size.height * 0.5697143,
        size.width * 0.2098571,
        size.height * 0.5730000,
        size.width * 0.2107143,
        size.height * 0.5768571);
    path_12.cubicTo(
        size.width * 0.2115714,
        size.height * 0.5807143,
        size.width * 0.2125714,
        size.height * 0.5844286,
        size.width * 0.2135714,
        size.height * 0.5880000);
    path_12.cubicTo(
        size.width * 0.2145714,
        size.height * 0.5915714,
        size.width * 0.2155714,
        size.height * 0.5948571,
        size.width * 0.2165714,
        size.height * 0.5980000);
    path_12.lineTo(size.width * 0.2195714, size.height * 0.6070000);
    path_12.cubicTo(
        size.width * 0.2205714,
        size.height * 0.6098571,
        size.width * 0.2215714,
        size.height * 0.6125714,
        size.width * 0.2224286,
        size.height * 0.6152857);
    path_12.lineTo(size.width * 0.2254286, size.height * 0.6227143);
    path_12.lineTo(size.width * 0.2282857, size.height * 0.6295714);
    path_12.cubicTo(
        size.width * 0.2292857,
        size.height * 0.6317143,
        size.width * 0.2301429,
        size.height * 0.6338571,
        size.width * 0.2311429,
        size.height * 0.6357143);
    path_12.cubicTo(
        size.width * 0.2321429,
        size.height * 0.6375714,
        size.width * 0.2330000,
        size.height * 0.6395714,
        size.width * 0.2338571,
        size.height * 0.6412857);
    path_12.cubicTo(
        size.width * 0.2347143,
        size.height * 0.6430000,
        size.width * 0.2355714,
        size.height * 0.6448571,
        size.width * 0.2364286,
        size.height * 0.6464286);
    path_12.lineTo(size.width * 0.2388571, size.height * 0.6511429);
    path_12.lineTo(size.width * 0.2412857, size.height * 0.6554286);
    path_12.arcToPoint(Offset(size.width * 0.2435714, size.height * 0.6594286),
        radius: Radius.elliptical(
            size.width * 0.03628571, size.height * 0.03628571),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_12.lineTo(size.width * 0.2455714, size.height * 0.6630000);
    path_12.lineTo(size.width * 0.2477143, size.height * 0.6662857);
    path_12.lineTo(size.width * 0.2495714, size.height * 0.6694286);
    path_12.arcToPoint(Offset(size.width * 0.2504286, size.height * 0.6728571),
        radius: Radius.elliptical(
            size.width * 0.01500000, size.height * 0.01500000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2504286, size.height * 0.6761429),
        radius: Radius.elliptical(
            size.width * 0.006571429, size.height * 0.006571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2488571, size.height * 0.6790000),
        radius: Radius.elliptical(
            size.width * 0.008000000, size.height * 0.008000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2432857, size.height * 0.6820000),
        radius: Radius.elliptical(
            size.width * 0.008714286, size.height * 0.008714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2400000, size.height * 0.6820000),
        radius: Radius.elliptical(
            size.width * 0.008285714, size.height * 0.008285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2371429, size.height * 0.6805714),
        radius: Radius.elliptical(
            size.width * 0.007714286, size.height * 0.007714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2350000, size.height * 0.6782857),
        radius: Radius.elliptical(
            size.width * 0.008142857, size.height * 0.008142857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.cubicTo(
        size.width * 0.2284286,
        size.height * 0.6675714,
        size.width * 0.2224286,
        size.height * 0.6565714,
        size.width * 0.2168571,
        size.height * 0.6452857);
    path_12.arcToPoint(Offset(size.width * 0.2041429, size.height * 0.6142857),
        radius:
            Radius.elliptical(size.width * 0.3011429, size.height * 0.3011429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.cubicTo(
        size.width * 0.2005714,
        size.height * 0.6044286,
        size.width * 0.1978571,
        size.height * 0.5950000,
        size.width * 0.1955714,
        size.height * 0.5857143);
    path_12.arcToPoint(Offset(size.width * 0.1901429, size.height * 0.5598571),
        radius:
            Radius.elliptical(size.width * 0.2394286, size.height * 0.2394286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.cubicTo(
        size.width * 0.1887143,
        size.height * 0.5517143,
        size.width * 0.1878571,
        size.height * 0.5438571,
        size.width * 0.1871429,
        size.height * 0.5364286);
    path_12.cubicTo(
        size.width * 0.1864286,
        size.height * 0.5290000,
        size.width * 0.1860000,
        size.height * 0.5221429,
        size.width * 0.1858571,
        size.height * 0.5151429);
    path_12.cubicTo(
        size.width * 0.1857143,
        size.height * 0.5081429,
        size.width * 0.1858571,
        size.height * 0.5021429,
        size.width * 0.1858571,
        size.height * 0.4960000);
    path_12.cubicTo(
        size.width * 0.1858571,
        size.height * 0.4898571,
        size.width * 0.1858571,
        size.height * 0.4841429,
        size.width * 0.1870000,
        size.height * 0.4787143);
    path_12.cubicTo(
        size.width * 0.1881429,
        size.height * 0.4732857,
        size.width * 0.1880000,
        size.height * 0.4680000,
        size.width * 0.1887143,
        size.height * 0.4630000);
    path_12.cubicTo(
        size.width * 0.1894286,
        size.height * 0.4580000,
        size.width * 0.1901429,
        size.height * 0.4532857,
        size.width * 0.1910000,
        size.height * 0.4487143);
    path_12.cubicTo(
        size.width * 0.1918571,
        size.height * 0.4441429,
        size.width * 0.1927143,
        size.height * 0.4398571,
        size.width * 0.1937143,
        size.height * 0.4358571);
    path_12.cubicTo(
        size.width * 0.1947143,
        size.height * 0.4318571,
        size.width * 0.1955714,
        size.height * 0.4278571,
        size.width * 0.1965714,
        size.height * 0.4242857);
    path_12.cubicTo(
        size.width * 0.1975714,
        size.height * 0.4207143,
        size.width * 0.1985714,
        size.height * 0.4171429,
        size.width * 0.1995714,
        size.height * 0.4137143);
    path_12.lineTo(size.width * 0.2025714, size.height * 0.4041429);
    path_12.cubicTo(
        size.width * 0.2035714,
        size.height * 0.4011429,
        size.width * 0.2047143,
        size.height * 0.3982857,
        size.width * 0.2057143,
        size.height * 0.3955714);
    path_12.lineTo(size.width * 0.2087143, size.height * 0.3878571);
    path_12.cubicTo(
        size.width * 0.2097143,
        size.height * 0.3852857,
        size.width * 0.2107143,
        size.height * 0.3828571,
        size.width * 0.2117143,
        size.height * 0.3807143);
    path_12.cubicTo(
        size.width * 0.2127143,
        size.height * 0.3785714,
        size.width * 0.2135714,
        size.height * 0.3762857,
        size.width * 0.2145714,
        size.height * 0.3741429);
    path_12.cubicTo(
        size.width * 0.2155714,
        size.height * 0.3720000,
        size.width * 0.2164286,
        size.height * 0.3701429,
        size.width * 0.2172857,
        size.height * 0.3682857);
    path_12.cubicTo(
        size.width * 0.2181429,
        size.height * 0.3664286,
        size.width * 0.2191429,
        size.height * 0.3645714,
        size.width * 0.2200000,
        size.height * 0.3628571);
    path_12.lineTo(size.width * 0.2225714, size.height * 0.3578571);
    path_12.cubicTo(
        size.width * 0.2232857,
        size.height * 0.3562857,
        size.width * 0.2241429,
        size.height * 0.3548571,
        size.width * 0.2250000,
        size.height * 0.3532857);
    path_12.arcToPoint(Offset(size.width * 0.2272857, size.height * 0.3492857),
        radius: Radius.elliptical(
            size.width * 0.02671429, size.height * 0.02671429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.lineTo(size.width * 0.2294286, size.height * 0.3454286);
    path_12.cubicTo(
        size.width * 0.2301429,
        size.height * 0.3442857,
        size.width * 0.2308571,
        size.height * 0.3431429,
        size.width * 0.2314286,
        size.height * 0.3420000);
    path_12.lineTo(size.width * 0.2334286, size.height * 0.3388571);
    path_12.arcToPoint(Offset(size.width * 0.2352857, size.height * 0.3360000),
        radius: Radius.elliptical(
            size.width * 0.01957143, size.height * 0.01957143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2378571, size.height * 0.3338571),
        radius: Radius.elliptical(
            size.width * 0.009285714, size.height * 0.009285714),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2410000, size.height * 0.3328571),
        radius: Radius.elliptical(
            size.width * 0.008142857, size.height * 0.008142857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2441429, size.height * 0.3328571),
        radius: Radius.elliptical(
            size.width * 0.007857143, size.height * 0.007857143),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2492857, size.height * 0.3365714),
        radius: Radius.elliptical(
            size.width * 0.008000000, size.height * 0.008000000),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2504286, size.height * 0.3395714),
        radius: Radius.elliptical(
            size.width * 0.007714286, size.height * 0.007714286),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2504286, size.height * 0.3428571),
        radius: Radius.elliptical(
            size.width * 0.006428571, size.height * 0.006428571),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.2494286, size.height * 0.3464286),
        radius: Radius.elliptical(
            size.width * 0.007142857, size.height * 0.007142857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.close();

    Paint paint_12_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.003571429;
    paint_12_stroke.color = Color(0xff000000).withOpacity(1.0);
    paint_12_stroke.strokeCap = StrokeCap.round;
    paint_12_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_12, paint_12_stroke);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.4810000, size.height * 0.09542857);
    path_13.arcToPoint(Offset(size.width * 0.4785714, size.height * 0.09285714),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.4785714, size.height * 0.09285714);
    path_13.lineTo(size.width * 0.4785714, size.height * 0.06571429);
    path_13.arcToPoint(Offset(size.width * 0.4810000, size.height * 0.06328571),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.4868571, size.height * 0.06328571);
    path_13.arcToPoint(Offset(size.width * 0.5175714, size.height * 0.06328571),
        radius: Radius.elliptical(
            size.width * 0.02242857, size.height * 0.02242857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.5234286, size.height * 0.06328571);
    path_13.arcToPoint(Offset(size.width * 0.5258571, size.height * 0.06571429),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.5258571, size.height * 0.06571429);
    path_13.lineTo(size.width * 0.5258571, size.height * 0.09285714);
    path_13.arcToPoint(Offset(size.width * 0.5234286, size.height * 0.09542857),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.5174286, size.height * 0.09542857);
    path_13.arcToPoint(Offset(size.width * 0.4870000, size.height * 0.09542857),
        radius: Radius.elliptical(
            size.width * 0.02471429, size.height * 0.02471429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.close();

    Paint paint_13_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.003571429;
    paint_13_stroke.color = Color(0xff000000).withOpacity(1.0);
    paint_13_stroke.strokeCap = StrokeCap.round;
    paint_13_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_13, paint_13_stroke);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.9045714, size.height * 0.4810000);
    path_14.arcToPoint(Offset(size.width * 0.9070000, size.height * 0.4785714),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.lineTo(size.width * 0.9342857, size.height * 0.4785714);
    path_14.arcToPoint(Offset(size.width * 0.9367143, size.height * 0.4810000),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.lineTo(size.width * 0.9367143, size.height * 0.4810000);
    path_14.lineTo(size.width * 0.9367143, size.height * 0.4868571);
    path_14.arcToPoint(Offset(size.width * 0.9367143, size.height * 0.5175714),
        radius: Radius.elliptical(
            size.width * 0.02242857, size.height * 0.02242857),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.lineTo(size.width * 0.9367143, size.height * 0.5234286);
    path_14.arcToPoint(Offset(size.width * 0.9342857, size.height * 0.5258571),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.lineTo(size.width * 0.9071429, size.height * 0.5258571);
    path_14.arcToPoint(Offset(size.width * 0.9045714, size.height * 0.5234286),
        radius: Radius.elliptical(
            size.width * 0.002571429, size.height * 0.002571429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.lineTo(size.width * 0.9045714, size.height * 0.5234286);
    path_14.lineTo(size.width * 0.9045714, size.height * 0.5174286);
    path_14.arcToPoint(Offset(size.width * 0.9045714, size.height * 0.4870000),
        radius: Radius.elliptical(
            size.width * 0.02471429, size.height * 0.02471429),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.close();

    Paint paint_14_stroke = Paint()
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.003571429;
    paint_14_stroke.color = Color(0xff000000).withOpacity(1.0);
    paint_14_stroke.strokeCap = StrokeCap.round;
    paint_14_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_14, paint_14_stroke);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
