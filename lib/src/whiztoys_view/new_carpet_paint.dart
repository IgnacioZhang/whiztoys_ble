import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import '../whiztoys/whiztoys.dart';
import 'led_state.dart';

//Add this CustomPaint widget to the Widget Tree
// CustomPaint(
//     size: Size(WIDTH, (WIDTH*0.9999689720438115).toDouble()), //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
//     painter: RPSCustomPainter(),
// )

//Copy this CustomPainter code to the Bottom of the File

class LedCarpetPainter extends CustomPainter {
  LedCarpetPainter(this._whizPuzzle)
      : leftTop = _whizPuzzle.leftTop,
        leftBottom = _whizPuzzle.leftBottom,
        rightBottom = _whizPuzzle.rightBottom,
        rightTop = _whizPuzzle.rightTop;
  WhizPuzzle _whizPuzzle;
  LedState leftTop;
  LedState leftBottom;
  LedState rightTop;
  LedState rightBottom;

  @override
  void paint(Canvas canvas, Size size) {
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.05447475, size.height * 0.03572463);
    path_0.arcToPoint(Offset(size.width * 0.03571318, size.height * 0.05446610),
        radius: Radius.elliptical(
            size.width * 0.01875123, size.height * 0.01875181),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.03571318, size.height * 0.05446610);
    path_0.lineTo(size.width * 0.03571318, size.height * 0.1800298);
    path_0.arcToPoint(Offset(size.width * 0.01458314, size.height * 0.1922242),
        radius: Radius.elliptical(
            size.width * 0.01407635, size.height * 0.01407679),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.007550136, size.height * 0.1881594);
    path_0.arcToPoint(
        Offset(size.width * 0.0005171326, size.height * 0.1922242),
        radius: Radius.elliptical(
            size.width * 0.004685221, size.height * 0.004685367),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.0005171326, size.height * 0.3238695);
    path_0.arcToPoint(Offset(size.width * 0.007550136, size.height * 0.3279240),
        radius: Radius.elliptical(
            size.width * 0.004685221, size.height * 0.004685367),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.01458314, size.height * 0.3238695);
    path_0.arcToPoint(Offset(size.width * 0.03571318, size.height * 0.3360432),
        radius: Radius.elliptical(
            size.width * 0.01406601, size.height * 0.01406644),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.03571318, size.height * 0.6633983);
    path_0.arcToPoint(Offset(size.width * 0.04275652, size.height * 0.6674631),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.04908623, size.height * 0.6638223);
    path_0.arcToPoint(Offset(size.width * 0.07021626, size.height * 0.6759960),
        radius: Radius.elliptical(
            size.width * 0.01407635, size.height * 0.01407679),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.07021626, size.height * 0.8079103);
    path_0.arcToPoint(Offset(size.width * 0.04908623, size.height * 0.8200943),
        radius: Radius.elliptical(
            size.width * 0.01407635, size.height * 0.01407679),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.04275652, size.height * 0.8164433);
    path_0.arcToPoint(Offset(size.width * 0.03571318, size.height * 0.8205080),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.03571318, size.height * 0.9455236);
    path_0.arcToPoint(Offset(size.width * 0.05446441, size.height * 0.9642857),
        radius: Radius.elliptical(
            size.width * 0.01876157, size.height * 0.01876215),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.1800242, size.height * 0.9642857);
    path_0.arcToPoint(Offset(size.width * 0.1922182, size.height * 0.9854061),
        radius: Radius.elliptical(
            size.width * 0.01406601, size.height * 0.01406644),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.1881535, size.height * 0.9924393);
    path_0.arcToPoint(Offset(size.width * 0.1922182, size.height * 0.9994829),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.3238595, size.height * 0.9994829);
    path_0.arcToPoint(Offset(size.width * 0.3279138, size.height * 0.9924393),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.3238595, size.height * 0.9854061);
    path_0.arcToPoint(Offset(size.width * 0.3360328, size.height * 0.9642857),
        radius: Radius.elliptical(
            size.width * 0.01405566, size.height * 0.01405610),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.6633880, size.height * 0.9642857);
    path_0.arcToPoint(Offset(size.width * 0.6674527, size.height * 0.9572525),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.6638018, size.height * 0.9509226);
    path_0.arcToPoint(Offset(size.width * 0.6759957, size.height * 0.9298333),
        radius: Radius.elliptical(
            size.width * 0.01406601, size.height * 0.01406644),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.8079059, size.height * 0.9298333);
    path_0.arcToPoint(Offset(size.width * 0.8200896, size.height * 0.9509640),
        radius: Radius.elliptical(
            size.width * 0.01406601, size.height * 0.01406644),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.8164490, size.height * 0.9572939);
    path_0.arcToPoint(Offset(size.width * 0.8205136, size.height * 0.9643271),
        radius: Radius.elliptical(
            size.width * 0.004685221, size.height * 0.004685367),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.9455149, size.height * 0.9643271);
    path_0.arcToPoint(Offset(size.width * 0.9642765, size.height * 0.9455649),
        radius: Radius.elliptical(
            size.width * 0.01876157, size.height * 0.01876215),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.9642765, size.height * 0.8199702);
    path_0.arcToPoint(Offset(size.width * 0.9854065, size.height * 0.8077862),
        radius: Radius.elliptical(
            size.width * 0.01406601, size.height * 0.01406644),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.9924395, size.height * 0.8118406);
    path_0.arcToPoint(Offset(size.width * 0.9988520, size.height * 0.8101237),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.arcToPoint(Offset(size.width * 0.9994829, size.height * 0.8077862),
        radius: Radius.elliptical(
            size.width * 0.004767963, size.height * 0.004768111),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.9994829, size.height * 0.6761408);
    path_0.arcToPoint(Offset(size.width * 0.9947770, size.height * 0.6714555),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.arcToPoint(Offset(size.width * 0.9924395, size.height * 0.6720760),
        radius: Radius.elliptical(
            size.width * 0.004757620, size.height * 0.004757768),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.9854065, size.height * 0.6761408);
    path_0.arcToPoint(Offset(size.width * 0.9642765, size.height * 0.6639465),
        radius: Radius.elliptical(
            size.width * 0.01407635, size.height * 0.01407679),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.9642765, size.height * 0.3365914);
    path_0.arcToPoint(Offset(size.width * 0.9572435, size.height * 0.3325369),
        radius: Radius.elliptical(
            size.width * 0.004685221, size.height * 0.004685367),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.9509138, size.height * 0.3361466);
    path_0.arcToPoint(Offset(size.width * 0.9298044, size.height * 0.3239936),
        radius: Radius.elliptical(
            size.width * 0.01407635, size.height * 0.01407679),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.9298044, size.height * 0.1920794);
    path_0.arcToPoint(Offset(size.width * 0.9509345, size.height * 0.1798953),
        radius: Radius.elliptical(
            size.width * 0.01407635, size.height * 0.01407679),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.9572642, size.height * 0.1835464);
    path_0.arcToPoint(Offset(size.width * 0.9636766, size.height * 0.1818295),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.arcToPoint(Offset(size.width * 0.9642972, size.height * 0.1794816),
        radius: Radius.elliptical(
            size.width * 0.004767963, size.height * 0.004768111),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.9642972, size.height * 0.05447644);
    path_0.arcToPoint(Offset(size.width * 0.9455356, size.height * 0.03572463),
        radius: Radius.elliptical(
            size.width * 0.01876157, size.height * 0.01876215),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.8199448, size.height * 0.03572463);
    path_0.arcToPoint(Offset(size.width * 0.8077611, size.height * 0.01459393),
        radius: Radius.elliptical(
            size.width * 0.01406601, size.height * 0.01406644),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.8118154, size.height * 0.007550370);
    path_0.arcToPoint(
        Offset(size.width * 0.8077611, size.height * 0.0005171486),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.6761095, size.height * 0.0005171486);
    path_0.arcToPoint(Offset(size.width * 0.6714243, size.height * 0.005171486),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.arcToPoint(Offset(size.width * 0.6720552, size.height * 0.007508998),
        radius: Radius.elliptical(
            size.width * 0.004602480, size.height * 0.004602623),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.6761095, size.height * 0.01455256);
    path_0.arcToPoint(Offset(size.width * 0.6639259, size.height * 0.03568326),
        radius: Radius.elliptical(
            size.width * 0.01407635, size.height * 0.01407679),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.3365809, size.height * 0.03568326);
    path_0.arcToPoint(Offset(size.width * 0.3325163, size.height * 0.04271648),
        radius: Radius.elliptical(
            size.width * 0.004685221, size.height * 0.004685367),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.lineTo(size.width * 0.3361362, size.height * 0.04909809);
    path_0.arcToPoint(Offset(size.width * 0.3239629, size.height * 0.07021844),
        radius: Radius.elliptical(
            size.width * 0.01405566, size.height * 0.01405610),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.1920734, size.height * 0.07021844);
    path_0.arcToPoint(Offset(size.width * 0.1798897, size.height * 0.04909809),
        radius: Radius.elliptical(
            size.width * 0.01405566, size.height * 0.01405610),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_0.lineTo(size.width * 0.1835304, size.height * 0.04275785);
    path_0.arcToPoint(Offset(size.width * 0.1794657, size.height * 0.03572463),
        radius: Radius.elliptical(
            size.width * 0.004695564, size.height * 0.004695710),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_0.close();

    //底部一層巧拼形狀
    Paint _paint = Paint()
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;
    _paint.color = Color(0xff444444).withOpacity(1.0);
    _paint.strokeCap = StrokeCap.round;
    _paint.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_0, _paint);
    canvas.drawPath(path_0, _whizPuzzle.paint);

    Path path_6 = Path();
    path_6.moveTo(size.width * 0.7437918, size.height * 0.3468723);
    path_6.arcToPoint(Offset(size.width * 0.7426852, size.height * 0.3440797),
        radius: Radius.elliptical(
            size.width * 0.007715618, size.height * 0.007715858),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7426852, size.height * 0.3410388),
        radius: Radius.elliptical(
            size.width * 0.008832625, size.height * 0.008832899),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7437918, size.height * 0.3381635),
        radius: Radius.elliptical(
            size.width * 0.008346520, size.height * 0.008346779),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7486632, size.height * 0.3345745),
        radius: Radius.elliptical(
            size.width * 0.008005213, size.height * 0.008005461),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7517660, size.height * 0.3343676),
        radius: Radius.elliptical(
            size.width * 0.008491317, size.height * 0.008491581),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7571028, size.height * 0.3373050),
        radius: Radius.elliptical(
            size.width * 0.008139667, size.height * 0.008139920),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7589231, size.height * 0.3400770),
        radius: Radius.elliptical(
            size.width * 0.02031297, size.height * 0.02031360),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.lineTo(size.width * 0.7607538, size.height * 0.3430971);
    path_6.lineTo(size.width * 0.7626982, size.height * 0.3463758);
    path_6.cubicTo(
        size.width * 0.7633705,
        size.height * 0.3475239,
        size.width * 0.7640531,
        size.height * 0.3487133,
        size.width * 0.7647667,
        size.height * 0.3499648);
    path_6.cubicTo(
        size.width * 0.7654804,
        size.height * 0.3512163,
        size.width * 0.7662250,
        size.height * 0.3525195,
        size.width * 0.7669490,
        size.height * 0.3538952);
    path_6.cubicTo(
        size.width * 0.7676730,
        size.height * 0.3552708,
        size.width * 0.7684590,
        size.height * 0.3566878,
        size.width * 0.7692554,
        size.height * 0.3581875);
    path_6.cubicTo(
        size.width * 0.7700518,
        size.height * 0.3596872,
        size.width * 0.7708585,
        size.height * 0.3612904,
        size.width * 0.7716756,
        size.height * 0.3628832);
    path_6.cubicTo(
        size.width * 0.7724927,
        size.height * 0.3644760,
        size.width * 0.7733408,
        size.height * 0.3662240,
        size.width * 0.7742096,
        size.height * 0.3679823);
    path_6.cubicTo(
        size.width * 0.7750783,
        size.height * 0.3697406,
        size.width * 0.7759575,
        size.height * 0.3716230,
        size.width * 0.7768469,
        size.height * 0.3736192);
    path_6.cubicTo(
        size.width * 0.7777364,
        size.height * 0.3756154,
        size.width * 0.7786569,
        size.height * 0.3776323,
        size.width * 0.7795774,
        size.height * 0.3798250);
    path_6.cubicTo(
        size.width * 0.7804979,
        size.height * 0.3820177,
        size.width * 0.7814494,
        size.height * 0.3842414,
        size.width * 0.7824009,
        size.height * 0.3866100);
    path_6.cubicTo(
        size.width * 0.7833525,
        size.height * 0.3889785,
        size.width * 0.7843143,
        size.height * 0.3914608,
        size.width * 0.7853176,
        size.height * 0.3940673);
    path_6.cubicTo(
        size.width * 0.7863208,
        size.height * 0.3966737,
        size.width * 0.7872620,
        size.height * 0.3994042,
        size.width * 0.7882342,
        size.height * 0.4022692);
    path_6.cubicTo(
        size.width * 0.7892064,
        size.height * 0.4051343,
        size.width * 0.7901786,
        size.height * 0.4081544,
        size.width * 0.7911405,
        size.height * 0.4113193);
    path_6.cubicTo(
        size.width * 0.7921024,
        size.height * 0.4144843,
        size.width * 0.7930642,
        size.height * 0.4178044,
        size.width * 0.7939951,
        size.height * 0.4212900);
    path_6.cubicTo(
        size.width * 0.7949259,
        size.height * 0.4247756,
        size.width * 0.7958464,
        size.height * 0.4284473,
        size.width * 0.7967152,
        size.height * 0.4322949);
    path_6.cubicTo(
        size.width * 0.7975840,
        size.height * 0.4361425,
        size.width * 0.7984424,
        size.height * 0.4401969,
        size.width * 0.7992388,
        size.height * 0.4444582);
    path_6.cubicTo(
        size.width * 0.8000352,
        size.height * 0.4487195,
        size.width * 0.8007695,
        size.height * 0.4531980,
        size.width * 0.8014211,
        size.height * 0.4579041);
    path_6.cubicTo(
        size.width * 0.8020727,
        size.height * 0.4626102,
        size.width * 0.8026622,
        size.height * 0.4675748,
        size.width * 0.8031276,
        size.height * 0.4727876);
    path_6.cubicTo(
        size.width * 0.8035930,
        size.height * 0.4780005,
        size.width * 0.8039654,
        size.height * 0.4834926,
        size.width * 0.8041619,
        size.height * 0.4892743);
    path_6.cubicTo(
        size.width * 0.8043584,
        size.height * 0.4950561,
        size.width * 0.8044204,
        size.height * 0.5011170,
        size.width * 0.8042550,
        size.height * 0.5074883);
    path_6.cubicTo(
        size.width * 0.8040895,
        size.height * 0.5138596,
        size.width * 0.8037378,
        size.height * 0.5206032,
        size.width * 0.8030862,
        size.height * 0.5276571);
    path_6.cubicTo(
        size.width * 0.8024347,
        size.height * 0.5347110,
        size.width * 0.8015142,
        size.height * 0.5421373,
        size.width * 0.8002213,
        size.height * 0.5499255);
    path_6.cubicTo(
        size.width * 0.7989285,
        size.height * 0.5577138,
        size.width * 0.7972633,
        size.height * 0.5658744,
        size.width * 0.7951224,
        size.height * 0.5744384);
    path_6.cubicTo(
        size.width * 0.7929815,
        size.height * 0.5830024,
        size.width * 0.7903027,
        size.height * 0.5919283,
        size.width * 0.7870034,
        size.height * 0.6013301);
    path_6.arcToPoint(Offset(size.width * 0.7749646, size.height * 0.6305387),
        radius:
            Radius.elliptical(size.width * 0.2883945, size.height * 0.2884035),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.quadraticBezierTo(size.width * 0.7671662, size.height * 0.6466220,
        size.width * 0.7577647, size.height * 0.6618779);
    path_6.arcToPoint(Offset(size.width * 0.7556962, size.height * 0.6640602),
        radius: Radius.elliptical(
            size.width * 0.007912129, size.height * 0.007912374),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7529244, size.height * 0.6653841),
        radius: Radius.elliptical(
            size.width * 0.008460289, size.height * 0.008460552),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7498216, size.height * 0.6656220),
        radius: Radius.elliptical(
            size.width * 0.008325835, size.height * 0.008326093),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7445365, size.height * 0.6627881),
        radius: Radius.elliptical(
            size.width * 0.008087954, size.height * 0.008088205),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7430161, size.height * 0.6601196),
        radius: Radius.elliptical(
            size.width * 0.008346520, size.height * 0.008346779),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7425921, size.height * 0.6570167),
        radius: Radius.elliptical(
            size.width * 0.007932814, size.height * 0.007933060),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7434505, size.height * 0.6538310),
        radius: Radius.elliptical(
            size.width * 0.008739541, size.height * 0.008739812),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.arcToPoint(Offset(size.width * 0.7452295, size.height * 0.6508523),
        radius: Radius.elliptical(
            size.width * 0.02544292, size.height * 0.02544371),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_6.cubicTo(
        size.width * 0.7458810,
        size.height * 0.6498180,
        size.width * 0.7465223,
        size.height * 0.6487837,
        size.width * 0.7471739,
        size.height * 0.6477494);
    path_6.cubicTo(
        size.width * 0.7478255,
        size.height * 0.6467151,
        size.width * 0.7484977,
        size.height * 0.6455153,
        size.width * 0.7491803,
        size.height * 0.6443155);
    path_6.cubicTo(
        size.width * 0.7498630,
        size.height * 0.6431157,
        size.width * 0.7505869,
        size.height * 0.6418746,
        size.width * 0.7512489,
        size.height * 0.6405713);
    path_6.cubicTo(
        size.width * 0.7519108,
        size.height * 0.6392681,
        size.width * 0.7527072,
        size.height * 0.6379132,
        size.width * 0.7534932,
        size.height * 0.6364962);
    path_6.cubicTo(
        size.width * 0.7542793,
        size.height * 0.6350792,
        size.width * 0.7550446,
        size.height * 0.6335795,
        size.width * 0.7558514,
        size.height * 0.6320384);
    path_6.cubicTo(
        size.width * 0.7566581,
        size.height * 0.6304973,
        size.width * 0.7574752,
        size.height * 0.6288528,
        size.width * 0.7583026,
        size.height * 0.6271462);
    path_6.cubicTo(
        size.width * 0.7591300,
        size.height * 0.6254396,
        size.width * 0.7599988,
        size.height * 0.6236606,
        size.width * 0.7608675,
        size.height * 0.6217885);
    path_6.cubicTo(
        size.width * 0.7617363,
        size.height * 0.6199164,
        size.width * 0.7626361,
        size.height * 0.6179616,
        size.width * 0.7635359,
        size.height * 0.6159137);
    path_6.cubicTo(
        size.width * 0.7644358,
        size.height * 0.6138658,
        size.width * 0.7653563,
        size.height * 0.6117041,
        size.width * 0.7662871,
        size.height * 0.6094493);
    path_6.cubicTo(
        size.width * 0.7672179,
        size.height * 0.6071946,
        size.width * 0.7681488,
        size.height * 0.6048260,
        size.width * 0.7690900,
        size.height * 0.6023437);
    path_6.cubicTo(
        size.width * 0.7700311,
        size.height * 0.5998614,
        size.width * 0.7709827,
        size.height * 0.5972446,
        size.width * 0.7719342,
        size.height * 0.5945244);
    path_6.cubicTo(
        size.width * 0.7728857,
        size.height * 0.5918042,
        size.width * 0.7738269,
        size.height * 0.5889185,
        size.width * 0.7747681,
        size.height * 0.5858984);
    path_6.cubicTo(
        size.width * 0.7757092,
        size.height * 0.5828782,
        size.width * 0.7766401,
        size.height * 0.5796926,
        size.width * 0.7775606,
        size.height * 0.5763932);
    path_6.cubicTo(
        size.width * 0.7784811,
        size.height * 0.5730938,
        size.width * 0.7793809,
        size.height * 0.5695668,
        size.width * 0.7802393,
        size.height * 0.5658847);
    path_6.cubicTo(
        size.width * 0.7810978,
        size.height * 0.5622026,
        size.width * 0.7819252,
        size.height * 0.5583447,
        size.width * 0.7827112,
        size.height * 0.5542799);
    path_6.cubicTo(
        size.width * 0.7834973,
        size.height * 0.5502151,
        size.width * 0.7842213,
        size.height * 0.5459331,
        size.width * 0.7849039,
        size.height * 0.5414443);
    path_6.cubicTo(
        size.width * 0.7855865,
        size.height * 0.5369554,
        size.width * 0.7861346,
        size.height * 0.5322080,
        size.width * 0.7866001,
        size.height * 0.5272330);
    path_6.cubicTo(
        size.width * 0.7870655,
        size.height * 0.5222581,
        size.width * 0.7874378,
        size.height * 0.5170349,
        size.width * 0.7876343,
        size.height * 0.5115117);
    path_6.cubicTo(
        size.width * 0.7878308,
        size.height * 0.5059886,
        size.width * 0.7879343,
        size.height * 0.5002069,
        size.width * 0.7877998,
        size.height * 0.4941045);
    path_6.cubicTo(
        size.width * 0.7876654,
        size.height * 0.4880022,
        size.width * 0.7873447,
        size.height * 0.4815895,
        size.width * 0.7867655,
        size.height * 0.4748562);
    path_6.cubicTo(
        size.width * 0.7861864,
        size.height * 0.4681230,
        size.width * 0.7853072,
        size.height * 0.4610070,
        size.width * 0.7840971,
        size.height * 0.4536118);
    path_6.cubicTo(
        size.width * 0.7828870,
        size.height * 0.4462165,
        size.width * 0.7813150,
        size.height * 0.4383766,
        size.width * 0.7792775,
        size.height * 0.4302160);
    path_6.cubicTo(
        size.width * 0.7772400,
        size.height * 0.4220554,
        size.width * 0.7747060,
        size.height * 0.4135224,
        size.width * 0.7715722,
        size.height * 0.4046068);
    path_6.arcToPoint(Offset(size.width * 0.7601849, size.height * 0.3766187),
        radius:
            Radius.elliptical(size.width * 0.2705328, size.height * 0.2705411),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_6.arcToPoint(Offset(size.width * 0.7437918, size.height * 0.3468723),
        radius:
            Radius.elliptical(size.width * 0.3339125, size.height * 0.3339229),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_6.close();

    // Paint paint_6_stroke = Paint()
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1;
    // paint_6_stroke.color = lineColor;
    // paint_6_stroke.strokeCap = StrokeCap.round;
    // paint_6_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_6, _whizPuzzle.paint);

    Path path_7 = Path();
    path_7.moveTo(size.width * 0.4798991, size.height * 0.1096355);
    path_7.arcToPoint(Offset(size.width * 0.4775513, size.height * 0.1072877),
        radius: Radius.elliptical(
            size.width * 0.002347782, size.height * 0.002347855),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_7.lineTo(size.width * 0.4775513, size.height * 0.08150263);
    path_7.arcToPoint(Offset(size.width * 0.4798991, size.height * 0.07915477),
        radius: Radius.elliptical(
            size.width * 0.002347782, size.height * 0.002347855),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_7.lineTo(size.width * 0.4854324, size.height * 0.07915477);
    path_7.arcToPoint(Offset(size.width * 0.5145780, size.height * 0.07915477),
        radius: Radius.elliptical(
            size.width * 0.02103695, size.height * 0.02103761),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_7.lineTo(size.width * 0.5201009, size.height * 0.07915477);
    path_7.arcToPoint(Offset(size.width * 0.5224487, size.height * 0.08149228),
        radius: Radius.elliptical(
            size.width * 0.002347782, size.height * 0.002347855),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_7.lineTo(size.width * 0.5224487, size.height * 0.08149228);
    path_7.lineTo(size.width * 0.5224487, size.height * 0.1072877);
    path_7.arcToPoint(Offset(size.width * 0.5201216, size.height * 0.1096355),
        radius: Radius.elliptical(
            size.width * 0.002337439, size.height * 0.002337512),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_7.lineTo(size.width * 0.5144435, size.height * 0.1096355);
    path_7.arcToPoint(Offset(size.width * 0.4855565, size.height * 0.1096355),
        radius: Radius.elliptical(
            size.width * 0.02346748, size.height * 0.02346821),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_7.close();

    // Paint paint_7_stroke = Paint()
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1;
    // paint_7_stroke.color = lineColor;
    // paint_7_stroke.strokeCap = StrokeCap.round;
    // paint_7_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_7, _whizPuzzle.paint);

    Path path_8 = Path();
    path_8.moveTo(size.width * 0.8903265, size.height * 0.4799139);
    path_8.arcToPoint(Offset(size.width * 0.8926640, size.height * 0.4775661),
        radius: Radius.elliptical(
            size.width * 0.002347782, size.height * 0.002347855),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_8.lineTo(size.width * 0.9184275, size.height * 0.4775661);
    path_8.arcToPoint(Offset(size.width * 0.9207753, size.height * 0.4799036),
        radius: Radius.elliptical(
            size.width * 0.002347782, size.height * 0.002347855),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_8.lineTo(size.width * 0.9207753, size.height * 0.4799036);
    path_8.lineTo(size.width * 0.9207753, size.height * 0.4854371);
    path_8.arcToPoint(Offset(size.width * 0.9207753, size.height * 0.5145836),
        radius: Radius.elliptical(
            size.width * 0.02105764, size.height * 0.02105829),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_8.lineTo(size.width * 0.9207753, size.height * 0.5201171);
    path_8.arcToPoint(Offset(size.width * 0.9184378, size.height * 0.5224649),
        radius: Radius.elliptical(
            size.width * 0.002347782, size.height * 0.002347855),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_8.lineTo(size.width * 0.8926640, size.height * 0.5224649);
    path_8.arcToPoint(Offset(size.width * 0.8903162, size.height * 0.5201378),
        radius: Radius.elliptical(
            size.width * 0.002337439, size.height * 0.002337512),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_8.lineTo(size.width * 0.8903162, size.height * 0.5144595);
    path_8.arcToPoint(Offset(size.width * 0.8903162, size.height * 0.4855716),
        radius: Radius.elliptical(
            size.width * 0.02343645, size.height * 0.02343718),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_8.close();

    // Paint paint_8_stroke = Paint()
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1;
    // paint_8_stroke.color = lineColor;
    // paint_8_stroke.strokeCap = StrokeCap.round;
    // paint_8_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_8, _whizPuzzle.paint);

    Path path_9 = Path();
    path_9.moveTo(size.width * 0.3468615, size.height * 0.7438149);
    path_9.cubicTo(
        size.width * 0.3560044,
        size.height * 0.7493898,
        size.width * 0.3653542,
        size.height * 0.7546750,
        size.width * 0.3749935,
        size.height * 0.7593294);
    path_9.arcToPoint(Offset(size.width * 0.4009536, size.height * 0.7703239),
        radius:
            Radius.elliptical(size.width * 0.2776795, size.height * 0.2776881),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_9.quadraticBezierTo(size.width * 0.4128373, size.height * 0.7746887,
        size.width * 0.4250830, size.height * 0.7779984);
    path_9.quadraticBezierTo(size.width * 0.4360772, size.height * 0.7809669,
        size.width * 0.4472783, size.height * 0.7830458);
    path_9.quadraticBezierTo(size.width * 0.4573728, size.height * 0.7849179,
        size.width * 0.4675810, size.height * 0.7860866);
    path_9.quadraticBezierTo(size.width * 0.4767239, size.height * 0.7871209,
        size.width * 0.4859288, size.height * 0.7875450);
    path_9.quadraticBezierTo(size.width * 0.4942857, size.height * 0.7879484,
        size.width * 0.5026839, size.height * 0.7878760);
    path_9.quadraticBezierTo(size.width * 0.5102858, size.height * 0.7878139,
        size.width * 0.5178773, size.height * 0.7873278);
    path_9.cubicTo(
        size.width * 0.5224694,
        size.height * 0.7870485,
        size.width * 0.5270409,
        size.height * 0.7866452,
        size.width * 0.5316227,
        size.height * 0.7861487);
    path_9.cubicTo(
        size.width * 0.5357597,
        size.height * 0.7856833,
        size.width * 0.5398968,
        size.height * 0.7851144,
        size.width * 0.5440338,
        size.height * 0.7845042);
    path_9.quadraticBezierTo(size.width * 0.5496602, size.height * 0.7836353,
        size.width * 0.5552246, size.height * 0.7825493);
    path_9.quadraticBezierTo(size.width * 0.5603959, size.height * 0.7815150,
        size.width * 0.5655052, size.height * 0.7803566);
    path_9.quadraticBezierTo(size.width * 0.5702835, size.height * 0.7792396,
        size.width * 0.5749791, size.height * 0.7779674);
    path_9.quadraticBezierTo(size.width * 0.5792919, size.height * 0.7768090,
        size.width * 0.5835738, size.height * 0.7755058);
    path_9.cubicTo(
        size.width * 0.5861698,
        size.height * 0.7747197,
        size.width * 0.5887451,
        size.height * 0.7738923,
        size.width * 0.5913204,
        size.height * 0.7730338);
    path_9.cubicTo(
        size.width * 0.5936475,
        size.height * 0.7722581,
        size.width * 0.5959746,
        size.height * 0.7714410,
        size.width * 0.5982914,
        size.height * 0.7706032);
    path_9.cubicTo(
        size.width * 0.6003599,
        size.height * 0.7698378,
        size.width * 0.6024905,
        size.height * 0.7690414,
        size.width * 0.6045797,
        size.height * 0.7682347);
    path_9.cubicTo(
        size.width * 0.6064828,
        size.height * 0.7675003,
        size.width * 0.6083651,
        size.height * 0.7667349,
        size.width * 0.6102475,
        size.height * 0.7659592);
    path_9.cubicTo(
        size.width * 0.6119540,
        size.height * 0.7652559,
        size.width * 0.6136502,
        size.height * 0.7645215,
        size.width * 0.6153464,
        size.height * 0.7637768);
    path_9.cubicTo(
        size.width * 0.6170426,
        size.height * 0.7630321,
        size.width * 0.6184492,
        size.height * 0.7624116,
        size.width * 0.6199592,
        size.height * 0.7617082);
    path_9.cubicTo(
        size.width * 0.6214693,
        size.height * 0.7610049,
        size.width * 0.6227518,
        size.height * 0.7604154,
        size.width * 0.6240963,
        size.height * 0.7597431);
    path_9.cubicTo(
        size.width * 0.6254409,
        size.height * 0.7590708,
        size.width * 0.6266406,
        size.height * 0.7585123,
        size.width * 0.6279127,
        size.height * 0.7578813);
    path_9.cubicTo(
        size.width * 0.6291849,
        size.height * 0.7572504,
        size.width * 0.6302605,
        size.height * 0.7567126,
        size.width * 0.6314189,
        size.height * 0.7561127);
    path_9.cubicTo(
        size.width * 0.6325773,
        size.height * 0.7555128,
        size.width * 0.6336012,
        size.height * 0.7549853,
        size.width * 0.6346768,
        size.height * 0.7544164);
    path_9.lineTo(size.width * 0.6377796, size.height * 0.7527616);
    path_9.lineTo(size.width * 0.6406859, size.height * 0.7511584);
    path_9.lineTo(size.width * 0.6434991, size.height * 0.7495553);
    path_9.lineTo(size.width * 0.6462606, size.height * 0.7479624);
    path_9.cubicTo(
        size.width * 0.6471915,
        size.height * 0.7474246,
        size.width * 0.6481119,
        size.height * 0.7468661,
        size.width * 0.6490221,
        size.height * 0.7462972);
    path_9.lineTo(size.width * 0.6518146, size.height * 0.7445493);
    path_9.arcToPoint(Offset(size.width * 0.6546175, size.height * 0.7430495),
        radius: Radius.elliptical(
            size.width * 0.01521404, size.height * 0.01521451),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.6575755, size.height * 0.7425324),
        radius: Radius.elliptical(
            size.width * 0.008212066, size.height * 0.008212321),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.6602542, size.height * 0.7430288),
        radius: Radius.elliptical(
            size.width * 0.008408576, size.height * 0.008408837),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.6625296, size.height * 0.7442907),
        radius: Radius.elliptical(
            size.width * 0.008160352, size.height * 0.008160606),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.6654876, size.height * 0.7489657),
        radius: Radius.elliptical(
            size.width * 0.008191380, size.height * 0.008191635),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.6649912, size.height * 0.7540234),
        radius: Radius.elliptical(
            size.width * 0.008522345, size.height * 0.008522610),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.6618884, size.height * 0.7576848),
        radius: Radius.elliptical(
            size.width * 0.008129325, size.height * 0.008129577),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.cubicTo(
        size.width * 0.6525800,
        size.height * 0.7633321,
        size.width * 0.6431888,
        size.height * 0.7687001,
        size.width * 0.6334668,
        size.height * 0.7734785);
    path_9.cubicTo(
        size.width * 0.6249754,
        size.height * 0.7776157,
        size.width * 0.6162566,
        size.height * 0.7813909,
        size.width * 0.6073929,
        size.height * 0.7847317);
    path_9.quadraticBezierTo(size.width * 0.5954161, size.height * 0.7892516,
        size.width * 0.5830774, size.height * 0.7927475);
    path_9.quadraticBezierTo(size.width * 0.5720521, size.height * 0.7958504,
        size.width * 0.5607889, size.height * 0.7981672);
    path_9.quadraticBezierTo(size.width * 0.5507359, size.height * 0.8002358,
        size.width * 0.5405484, size.height * 0.8015804);
    path_9.quadraticBezierTo(size.width * 0.5314572, size.height * 0.8028009,
        size.width * 0.5222832, size.height * 0.8034732);
    path_9.quadraticBezierTo(size.width * 0.5140091, size.height * 0.8040731,
        size.width * 0.5057867, size.height * 0.8042075);
    path_9.quadraticBezierTo(size.width * 0.4982779, size.height * 0.8043420,
        size.width * 0.4907795, size.height * 0.8041144);
    path_9.quadraticBezierTo(size.width * 0.4838086, size.height * 0.8039179,
        size.width * 0.4768583, size.height * 0.8033904);
    path_9.quadraticBezierTo(size.width * 0.4704045, size.height * 0.8028940,
        size.width * 0.4639817, size.height * 0.8021286);
    path_9.quadraticBezierTo(size.width * 0.4580347, size.height * 0.8013942,
        size.width * 0.4521497, size.height * 0.8004841);
    path_9.quadraticBezierTo(size.width * 0.4467095, size.height * 0.7996152,
        size.width * 0.4413210, size.height * 0.7985603);
    path_9.cubicTo(
        size.width * 0.4380217,
        size.height * 0.7979087,
        size.width * 0.4347224,
        size.height * 0.7972053,
        size.width * 0.4314437,
        size.height * 0.7964917);
    path_9.cubicTo(
        size.width * 0.4284444,
        size.height * 0.7957883,
        size.width * 0.4254553,
        size.height * 0.7950643,
        size.width * 0.4224767,
        size.height * 0.7942990);
    path_9.quadraticBezierTo(size.width * 0.4183396, size.height * 0.7932647,
        size.width * 0.4143370, size.height * 0.7920338);
    path_9.cubicTo(
        size.width * 0.4118754,
        size.height * 0.7913098,
        size.width * 0.4094139,
        size.height * 0.7905548,
        size.width * 0.4069730,
        size.height * 0.7897791);
    path_9.cubicTo(
        size.width * 0.4047493,
        size.height * 0.7890654,
        size.width * 0.4025257,
        size.height * 0.7883311,
        size.width * 0.4003227,
        size.height * 0.7875864);
    path_9.cubicTo(
        size.width * 0.3983162,
        size.height * 0.7868934,
        size.width * 0.3963201,
        size.height * 0.7861590,
        size.width * 0.3943240,
        size.height * 0.7854040);
    path_9.quadraticBezierTo(size.width * 0.3916142, size.height * 0.7843697,
        size.width * 0.3889251, size.height * 0.7833354);
    path_9.quadraticBezierTo(size.width * 0.3864739, size.height * 0.7823735,
        size.width * 0.3840537, size.height * 0.7813702);
    path_9.quadraticBezierTo(size.width * 0.3818611, size.height * 0.7804704,
        size.width * 0.3796788, size.height * 0.7795292);
    path_9.cubicTo(
        size.width * 0.3783549,
        size.height * 0.7789603,
        size.width * 0.3770414,
        size.height * 0.7783811,
        size.width * 0.3757279,
        size.height * 0.7777916);
    path_9.cubicTo(
        size.width * 0.3744143,
        size.height * 0.7772020,
        size.width * 0.3733387,
        size.height * 0.7767573,
        size.width * 0.3721493,
        size.height * 0.7761677);
    path_9.cubicTo(
        size.width * 0.3709599,
        size.height * 0.7755782,
        size.width * 0.3699773,
        size.height * 0.7751334,
        size.width * 0.3689017,
        size.height * 0.7746370);
    path_9.lineTo(size.width * 0.3659334, size.height * 0.7731786);
    path_9.lineTo(size.width * 0.3631926, size.height * 0.7718237);
    path_9.lineTo(size.width * 0.3605655, size.height * 0.7705515);
    path_9.lineTo(size.width * 0.3581764, size.height * 0.7693000);
    path_9.lineTo(size.width * 0.3558803, size.height * 0.7680795);
    path_9.lineTo(size.width * 0.3536463, size.height * 0.7668694);
    path_9.lineTo(size.width * 0.3514640, size.height * 0.7656386);
    path_9.lineTo(size.width * 0.3492403, size.height * 0.7643457);
    path_9.lineTo(size.width * 0.3469546, size.height * 0.7630321);
    path_9.lineTo(size.width * 0.3445861, size.height * 0.7616462);
    path_9.lineTo(size.width * 0.3420729, size.height * 0.7601258);
    path_9.lineTo(size.width * 0.3393217, size.height * 0.7584502);
    path_9.arcToPoint(Offset(size.width * 0.3368395, size.height * 0.7566712),
        radius: Radius.elliptical(
            size.width * 0.01447971, size.height * 0.01448016),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3351226, size.height * 0.7543027),
        radius: Radius.elliptical(
            size.width * 0.007953499, size.height * 0.007953746),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3344917, size.height * 0.7491312),
        radius: Radius.elliptical(
            size.width * 0.008274122, size.height * 0.008274378),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3355260, size.height * 0.7464420),
        radius: Radius.elliptical(
            size.width * 0.008387891, size.height * 0.008388151),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3373670, size.height * 0.7443734),
        radius: Radius.elliptical(
            size.width * 0.008274122, size.height * 0.008274378),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3396527, size.height * 0.7430909),
        radius: Radius.elliptical(
            size.width * 0.008025898, size.height * 0.008026147),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3421660, size.height * 0.7425737),
        radius: Radius.elliptical(
            size.width * 0.008377548, size.height * 0.008377808),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3446585, size.height * 0.7428530),
        radius: Radius.elliptical(
            size.width * 0.008470632, size.height * 0.008470895),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.arcToPoint(Offset(size.width * 0.3468615, size.height * 0.7438149),
        radius: Radius.elliptical(
            size.width * 0.007984527, size.height * 0.007984775),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_9.close();

    // Paint paint_9_stroke = Paint()
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1;
    // paint_9_stroke.color = lineColor;
    // paint_9_stroke.strokeCap = StrokeCap.round;
    // paint_9_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_9, _whizPuzzle.paint);

    Path path_10 = Path();
    path_10.moveTo(size.width * 0.3468615, size.height * 0.2561748);
    path_10.arcToPoint(Offset(size.width * 0.3446792, size.height * 0.2571573),
        radius: Radius.elliptical(
            size.width * 0.007984527, size.height * 0.007984775),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3421866, size.height * 0.2574159),
        radius: Radius.elliptical(
            size.width * 0.008118982, size.height * 0.008119234),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3373877, size.height * 0.2556266),
        radius: Radius.elliptical(
            size.width * 0.008336178, size.height * 0.008336436),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3344917, size.height * 0.2508792),
        radius: Radius.elliptical(
            size.width * 0.008067269, size.height * 0.008067519),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3351226, size.height * 0.2457077),
        radius: Radius.elliptical(
            size.width * 0.008274122, size.height * 0.008274378),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3368395, size.height * 0.2433391),
        radius: Radius.elliptical(
            size.width * 0.007953499, size.height * 0.007953746),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.3393217, size.height * 0.2415601),
        radius: Radius.elliptical(
            size.width * 0.01394189, size.height * 0.01394233),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.lineTo(size.width * 0.3420729, size.height * 0.2398846);
    path_10.lineTo(size.width * 0.3445861, size.height * 0.2383642);
    path_10.lineTo(size.width * 0.3469546, size.height * 0.2369782);
    path_10.lineTo(size.width * 0.3492403, size.height * 0.2356646);
    path_10.lineTo(size.width * 0.3514640, size.height * 0.2344131);
    path_10.lineTo(size.width * 0.3536463, size.height * 0.2331823);
    path_10.lineTo(size.width * 0.3558803, size.height * 0.2319722);
    path_10.lineTo(size.width * 0.3581764, size.height * 0.2307517);
    path_10.lineTo(size.width * 0.3605655, size.height * 0.2295002);
    path_10.lineTo(size.width * 0.3631098, size.height * 0.2281970);
    path_10.lineTo(size.width * 0.3658506, size.height * 0.2268421);
    path_10.lineTo(size.width * 0.3688190, size.height * 0.2253837);
    path_10.lineTo(size.width * 0.3720666, size.height * 0.2238530);
    path_10.lineTo(size.width * 0.3756451, size.height * 0.2222188);
    path_10.cubicTo(
        size.width * 0.3769586,
        size.height * 0.2216292,
        size.width * 0.3782825,
        size.height * 0.2210500,
        size.width * 0.3795960,
        size.height * 0.2204812);
    path_10.cubicTo(
        size.width * 0.3809095,
        size.height * 0.2199123,
        size.width * 0.3825126,
        size.height * 0.2192400,
        size.width * 0.3839710,
        size.height * 0.2186401);
    path_10.cubicTo(
        size.width * 0.3854293,
        size.height * 0.2180402,
        size.width * 0.3872082,
        size.height * 0.2173265,
        size.width * 0.3888423,
        size.height * 0.2166853);
    path_10.cubicTo(
        size.width * 0.3906316,
        size.height * 0.2159820,
        size.width * 0.3924312,
        size.height * 0.2152890,
        size.width * 0.3942412,
        size.height * 0.2146167);
    path_10.cubicTo(
        size.width * 0.3962270,
        size.height * 0.2138823,
        size.width * 0.3982335,
        size.height * 0.2131480,
        size.width * 0.4002399,
        size.height * 0.2124343);
    path_10.quadraticBezierTo(size.width * 0.4035599, size.height * 0.2112863,
        size.width * 0.4068903, size.height * 0.2102416);
    path_10.quadraticBezierTo(size.width * 0.4105309, size.height * 0.2090625,
        size.width * 0.4142542, size.height * 0.2079765);
    path_10.cubicTo(
        size.width * 0.4169537,
        size.height * 0.2071904,
        size.width * 0.4196738,
        size.height * 0.2064354,
        size.width * 0.4223939,
        size.height * 0.2057114);
    path_10.quadraticBezierTo(size.width * 0.4268619, size.height * 0.2045323,
        size.width * 0.4313610, size.height * 0.2035290);
    path_10.cubicTo(
        size.width * 0.4346396,
        size.height * 0.2027740,
        size.width * 0.4379389,
        size.height * 0.2020707,
        size.width * 0.4412382,
        size.height * 0.2014604);
    path_10.quadraticBezierTo(size.width * 0.4466164, size.height * 0.2004261,
        size.width * 0.4520670, size.height * 0.1995366);
    path_10.quadraticBezierTo(size.width * 0.4579623, size.height * 0.1985851,
        size.width * 0.4638990, size.height * 0.1978818);
    path_10.quadraticBezierTo(size.width * 0.4703218, size.height * 0.1971267,
        size.width * 0.4767756, size.height * 0.1966303);
    path_10.quadraticBezierTo(size.width * 0.4837258, size.height * 0.1960821,
        size.width * 0.4906968, size.height * 0.1958959);
    path_10.quadraticBezierTo(size.width * 0.4981849, size.height * 0.1956684,
        size.width * 0.5057040, size.height * 0.1958132);
    path_10.quadraticBezierTo(size.width * 0.5139781, size.height * 0.1959476,
        size.width * 0.5222005, size.height * 0.1965372);
    path_10.quadraticBezierTo(size.width * 0.5313537, size.height * 0.1971991,
        size.width * 0.5404760, size.height * 0.1984299);
    path_10.quadraticBezierTo(size.width * 0.5506635, size.height * 0.1997952,
        size.width * 0.5607062, size.height * 0.2018535);
    path_10.quadraticBezierTo(size.width * 0.5719383, size.height * 0.2041496,
        size.width * 0.5829946, size.height * 0.2072732);
    path_10.quadraticBezierTo(size.width * 0.5953127, size.height * 0.2107794,
        size.width * 0.6073102, size.height * 0.2152890);
    path_10.quadraticBezierTo(size.width * 0.6205591, size.height * 0.2202433,
        size.width * 0.6333840, size.height * 0.2265111);
    path_10.cubicTo(
        size.width * 0.6431164,
        size.height * 0.2312999,
        size.width * 0.6525593,
        size.height * 0.2366576,
        size.width * 0.6618056,
        size.height * 0.2423152);
    path_10.arcToPoint(Offset(size.width * 0.6649084, size.height * 0.2459766),
        radius: Radius.elliptical(
            size.width * 0.008129325, size.height * 0.008129577),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6654049, size.height * 0.2510343),
        radius: Radius.elliptical(
            size.width * 0.008522345, size.height * 0.008522610),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6643706, size.height * 0.2535683),
        radius: Radius.elliptical(
            size.width * 0.007984527, size.height * 0.007984775),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6574927, size.height * 0.2574573),
        radius: Radius.elliptical(
            size.width * 0.008274122, size.height * 0.008274378),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6545347, size.height * 0.2569505),
        radius: Radius.elliptical(
            size.width * 0.007984527, size.height * 0.007984775),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.arcToPoint(Offset(size.width * 0.6517319, size.height * 0.2554507),
        radius: Radius.elliptical(
            size.width * 0.01521404, size.height * 0.01521451),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_10.lineTo(size.width * 0.6489394, size.height * 0.2537028);
    path_10.cubicTo(
        size.width * 0.6480085,
        size.height * 0.2531236,
        size.width * 0.6471087,
        size.height * 0.2525754,
        size.width * 0.6461779,
        size.height * 0.2520376);
    path_10.lineTo(size.width * 0.6434164, size.height * 0.2504344);
    path_10.lineTo(size.width * 0.6406032, size.height * 0.2488416);
    path_10.lineTo(size.width * 0.6377486, size.height * 0.2471971);
    path_10.lineTo(size.width * 0.6346458, size.height * 0.2455525);
    path_10.lineTo(size.width * 0.6313982, size.height * 0.2438563);
    path_10.cubicTo(
        size.width * 0.6302295,
        size.height * 0.2432564,
        size.width * 0.6290608,
        size.height * 0.2426668,
        size.width * 0.6278817,
        size.height * 0.2420876);
    path_10.cubicTo(
        size.width * 0.6267027,
        size.height * 0.2415084,
        size.width * 0.6253374,
        size.height * 0.2408361,
        size.width * 0.6240653,
        size.height * 0.2402259);
    path_10.cubicTo(
        size.width * 0.6227931,
        size.height * 0.2396157,
        size.width * 0.6212831,
        size.height * 0.2389123,
        size.width * 0.6199282,
        size.height * 0.2382711);
    path_10.cubicTo(
        size.width * 0.6185733,
        size.height * 0.2376298,
        size.width * 0.6168254,
        size.height * 0.2368748,
        size.width * 0.6153154,
        size.height * 0.2362025);
    path_10.cubicTo(
        size.width * 0.6138054,
        size.height * 0.2355302,
        size.width * 0.6119230,
        size.height * 0.2347338,
        size.width * 0.6102165,
        size.height * 0.2340098);
    path_10.cubicTo(
        size.width * 0.6083341,
        size.height * 0.2332340,
        size.width * 0.6064517,
        size.height * 0.2324790,
        size.width * 0.6045487,
        size.height * 0.2317447);
    path_10.cubicTo(
        size.width * 0.6024802,
        size.height * 0.2309276,
        size.width * 0.6004116,
        size.height * 0.2301311,
        size.width * 0.5982604,
        size.height * 0.2293761);
    path_10.cubicTo(
        size.width * 0.5959436,
        size.height * 0.2285280,
        size.width * 0.5936269,
        size.height * 0.2277109,
        size.width * 0.5912894,
        size.height * 0.2269352);
    path_10.cubicTo(
        size.width * 0.5887141,
        size.height * 0.2260767,
        size.width * 0.5861181,
        size.height * 0.2252596,
        size.width * 0.5835428,
        size.height * 0.2244735);
    path_10.quadraticBezierTo(size.width * 0.5792609, size.height * 0.2231703,
        size.width * 0.5749480, size.height * 0.2220119);
    path_10.quadraticBezierTo(size.width * 0.5702214, size.height * 0.2207190,
        size.width * 0.5654742, size.height * 0.2196227);
    path_10.cubicTo(
        size.width * 0.5620818,
        size.height * 0.2188263,
        size.width * 0.5586480,
        size.height * 0.2181023,
        size.width * 0.5552039,
        size.height * 0.2174403);
    path_10.cubicTo(
        size.width * 0.5514806,
        size.height * 0.2167060,
        size.width * 0.5477572,
        size.height * 0.2160544,
        size.width * 0.5440028,
        size.height * 0.2154752);
    path_10.quadraticBezierTo(size.width * 0.5377972, size.height * 0.2145236,
        size.width * 0.5315916, size.height * 0.2138306);
    path_10.quadraticBezierTo(size.width * 0.5247345, size.height * 0.2131066,
        size.width * 0.5178462, size.height * 0.2126412);
    path_10.quadraticBezierTo(size.width * 0.5102651, size.height * 0.2121757,
        size.width * 0.5026529, size.height * 0.2121033);
    path_10.quadraticBezierTo(size.width * 0.4942753, size.height * 0.2120309,
        size.width * 0.4858978, size.height * 0.2124343);
    path_10.quadraticBezierTo(size.width * 0.4766928, size.height * 0.2128894,
        size.width * 0.4675499, size.height * 0.2138927);
    path_10.quadraticBezierTo(size.width * 0.4573417, size.height * 0.2150408,
        size.width * 0.4472473, size.height * 0.2169232);
    path_10.quadraticBezierTo(size.width * 0.4360669, size.height * 0.2189918,
        size.width * 0.4250623, size.height * 0.2219809);
    path_10.quadraticBezierTo(size.width * 0.4128373, size.height * 0.2252700,
        size.width * 0.4009329, size.height * 0.2296347);
    path_10.arcToPoint(Offset(size.width * 0.3749729, size.height * 0.2406189),
        radius:
            Radius.elliptical(size.width * 0.2775968, size.height * 0.2776054),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_10.cubicTo(
        size.width * 0.3653438,
        size.height * 0.2453250,
        size.width * 0.3560044,
        size.height * 0.2505999,
        size.width * 0.3468615,
        size.height * 0.2561748);
    path_10.close();

    // Paint paint_10_stroke = Paint()
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1;
    // paint_10_stroke.color = lineColor;
    // paint_10_stroke.strokeCap = StrokeCap.round;
    // paint_10_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_10, _whizPuzzle.paint);

    Path path_14 = Path();
    path_14.moveTo(size.width * 0.2561668, size.height * 0.3468723);
    path_14.quadraticBezierTo(size.width * 0.2472618, size.height * 0.3613524,
        size.width * 0.2398358, size.height * 0.3766704);
    path_14.arcToPoint(Offset(size.width * 0.2283864, size.height * 0.4045240),
        radius:
            Radius.elliptical(size.width * 0.2739355, size.height * 0.2739440),
        rotation: 0,
        largeArc: false,
        clockwise: false);
    path_14.cubicTo(
        size.width * 0.2252836,
        size.height * 0.4134397,
        size.width * 0.2227290,
        size.height * 0.4219726,
        size.width * 0.2206812,
        size.height * 0.4301332);
    path_14.cubicTo(
        size.width * 0.2186333,
        size.height * 0.4382938,
        size.width * 0.2170406,
        size.height * 0.4460924,
        size.width * 0.2158615,
        size.height * 0.4535187);
    path_14.cubicTo(
        size.width * 0.2146824,
        size.height * 0.4609449,
        size.width * 0.2137930,
        size.height * 0.4679988,
        size.width * 0.2131931,
        size.height * 0.4747631);
    path_14.cubicTo(
        size.width * 0.2125932,
        size.height * 0.4815275,
        size.width * 0.2122726,
        size.height * 0.4878780,
        size.width * 0.2121588,
        size.height * 0.4940218);
    path_14.cubicTo(
        size.width * 0.2120451,
        size.height * 0.5001655,
        size.width * 0.2121071,
        size.height * 0.5059058,
        size.width * 0.2123346,
        size.height * 0.5114186);
    path_14.cubicTo(
        size.width * 0.2125622,
        size.height * 0.5169314,
        size.width * 0.2129138,
        size.height * 0.5221650,
        size.width * 0.2133689,
        size.height * 0.5271503);
    path_14.cubicTo(
        size.width * 0.2138240,
        size.height * 0.5321356,
        size.width * 0.2144032,
        size.height * 0.5368624,
        size.width * 0.2150754,
        size.height * 0.5413512);
    path_14.cubicTo(
        size.width * 0.2157477,
        size.height * 0.5458401,
        size.width * 0.2164510,
        size.height * 0.5501014,
        size.width * 0.2172577,
        size.height * 0.5541972);
    path_14.cubicTo(
        size.width * 0.2180645,
        size.height * 0.5582930,
        size.width * 0.2188712,
        size.height * 0.5621199,
        size.width * 0.2197296,
        size.height * 0.5658020);
    path_14.cubicTo(
        size.width * 0.2205881,
        size.height * 0.5694841,
        size.width * 0.2214982,
        size.height * 0.5729800,
        size.width * 0.2224084,
        size.height * 0.5763001);
    path_14.cubicTo(
        size.width * 0.2233185,
        size.height * 0.5796202,
        size.width * 0.2242597,
        size.height * 0.5827955,
        size.width * 0.2252009,
        size.height * 0.5858156);
    path_14.cubicTo(
        size.width * 0.2261421,
        size.height * 0.5888358,
        size.width * 0.2270936,
        size.height * 0.5917008,
        size.width * 0.2280348,
        size.height * 0.5944313);
    path_14.cubicTo(
        size.width * 0.2289760,
        size.height * 0.5971619,
        size.width * 0.2299378,
        size.height * 0.5997787,
        size.width * 0.2308790,
        size.height * 0.6022610);
    path_14.cubicTo(
        size.width * 0.2318202,
        size.height * 0.6047433,
        size.width * 0.2327614,
        size.height * 0.6071015,
        size.width * 0.2336819,
        size.height * 0.6093666);
    path_14.cubicTo(
        size.width * 0.2346024,
        size.height * 0.6116317,
        size.width * 0.2355332,
        size.height * 0.6137313,
        size.width * 0.2364330,
        size.height * 0.6158206);
    path_14.cubicTo(
        size.width * 0.2373328,
        size.height * 0.6179099,
        size.width * 0.2382223,
        size.height * 0.6198337,
        size.width * 0.2391014,
        size.height * 0.6217058);
    path_14.cubicTo(
        size.width * 0.2399806,
        size.height * 0.6235778,
        size.width * 0.2408287,
        size.height * 0.6253465,
        size.width * 0.2416664,
        size.height * 0.6270634);
    path_14.cubicTo(
        size.width * 0.2425042,
        size.height * 0.6287804,
        size.width * 0.2433212,
        size.height * 0.6303939,
        size.width * 0.2441176,
        size.height * 0.6319453);
    path_14.cubicTo(
        size.width * 0.2449140,
        size.height * 0.6334968,
        size.width * 0.2457104,
        size.height * 0.6349861,
        size.width * 0.2464757,
        size.height * 0.6364031);
    path_14.cubicTo(
        size.width * 0.2472411,
        size.height * 0.6378201,
        size.width * 0.2479858,
        size.height * 0.6391854,
        size.width * 0.2487098,
        size.height * 0.6405403);
    path_14.cubicTo(
        size.width * 0.2494337,
        size.height * 0.6418952,
        size.width * 0.2501681,
        size.height * 0.6430847,
        size.width * 0.2508300,
        size.height * 0.6442741);
    path_14.cubicTo(
        size.width * 0.2514919,
        size.height * 0.6454636,
        size.width * 0.2521952,
        size.height * 0.6466116,
        size.width * 0.2528365,
        size.height * 0.6477183);
    path_14.cubicTo(
        size.width * 0.2534777,
        size.height * 0.6488250,
        size.width * 0.2541396,
        size.height * 0.6498697,
        size.width * 0.2547809,
        size.height * 0.6508212);
    path_14.arcToPoint(Offset(size.width * 0.2565702, size.height * 0.6538000),
        radius: Radius.elliptical(
            size.width * 0.02552567, size.height * 0.02552646),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2574286, size.height * 0.6569960),
        radius: Radius.elliptical(
            size.width * 0.008842968, size.height * 0.008843242),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2570046, size.height * 0.6600989),
        radius: Radius.elliptical(
            size.width * 0.007860416, size.height * 0.007860659),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2554842, size.height * 0.6627777),
        radius: Radius.elliptical(
            size.width * 0.008274122, size.height * 0.008274378),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2501991, size.height * 0.6656117),
        radius: Radius.elliptical(
            size.width * 0.008087954, size.height * 0.008088205),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2470963, size.height * 0.6653738),
        radius: Radius.elliptical(
            size.width * 0.008367206, size.height * 0.008367465),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2443348, size.height * 0.6640499),
        radius: Radius.elliptical(
            size.width * 0.008522345, size.height * 0.008522610),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2422663, size.height * 0.6618572),
        radius: Radius.elliptical(
            size.width * 0.007808702, size.height * 0.007808945),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2250665, size.height * 0.6305283),
        radius:
            Radius.elliptical(size.width * 0.3566767, size.height * 0.3566878),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2130586, size.height * 0.6013301),
        radius:
            Radius.elliptical(size.width * 0.2895943, size.height * 0.2896032),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.cubicTo(
        size.width * 0.2097697,
        size.height * 0.5920214,
        size.width * 0.2071116,
        size.height * 0.5830334,
        size.width * 0.2049500,
        size.height * 0.5744384);
    path_14.cubicTo(
        size.width * 0.2027884,
        size.height * 0.5658434,
        size.width * 0.2011232,
        size.height * 0.5576724,
        size.width * 0.1998511,
        size.height * 0.5499152);
    path_14.cubicTo(
        size.width * 0.1985789,
        size.height * 0.5421580,
        size.width * 0.1976584,
        size.height * 0.5347110,
        size.width * 0.1969862,
        size.height * 0.5276468);
    path_14.cubicTo(
        size.width * 0.1963139,
        size.height * 0.5205825,
        size.width * 0.1959519,
        size.height * 0.5138699,
        size.width * 0.1958071,
        size.height * 0.5074883);
    path_14.cubicTo(
        size.width * 0.1956623,
        size.height * 0.5011067,
        size.width * 0.1957037,
        size.height * 0.4950250,
        size.width * 0.1959105,
        size.height * 0.4892640);
    path_14.cubicTo(
        size.width * 0.1961174,
        size.height * 0.4835030,
        size.width * 0.1964690,
        size.height * 0.4780108,
        size.width * 0.1969448,
        size.height * 0.4727876);
    path_14.cubicTo(
        size.width * 0.1974205,
        size.height * 0.4675644,
        size.width * 0.1979790,
        size.height * 0.4625895,
        size.width * 0.1986513,
        size.height * 0.4579041);
    path_14.cubicTo(
        size.width * 0.1993236,
        size.height * 0.4532187,
        size.width * 0.2000476,
        size.height * 0.4487092,
        size.width * 0.2008336,
        size.height * 0.4444582);
    path_14.cubicTo(
        size.width * 0.2016197,
        size.height * 0.4402073,
        size.width * 0.2024781,
        size.height * 0.4361839,
        size.width * 0.2033469,
        size.height * 0.4322949);
    path_14.cubicTo(
        size.width * 0.2042157,
        size.height * 0.4284059,
        size.width * 0.2051362,
        size.height * 0.4247756,
        size.width * 0.2060773,
        size.height * 0.4212900);
    path_14.cubicTo(
        size.width * 0.2070185,
        size.height * 0.4178044,
        size.width * 0.2079700,
        size.height * 0.4144740,
        size.width * 0.2089319,
        size.height * 0.4113090);
    path_14.cubicTo(
        size.width * 0.2098938,
        size.height * 0.4081441,
        size.width * 0.2108763,
        size.height * 0.4051032,
        size.width * 0.2118485,
        size.height * 0.4022692);
    path_14.cubicTo(
        size.width * 0.2128208,
        size.height * 0.3994353,
        size.width * 0.2137930,
        size.height * 0.3966634,
        size.width * 0.2147652,
        size.height * 0.3940673);
    path_14.cubicTo(
        size.width * 0.2157374,
        size.height * 0.3914712,
        size.width * 0.2166992,
        size.height * 0.3889682,
        size.width * 0.2176818,
        size.height * 0.3865996);
    path_14.cubicTo(
        size.width * 0.2186643,
        size.height * 0.3842311,
        size.width * 0.2195745,
        size.height * 0.3819763,
        size.width * 0.2204950,
        size.height * 0.3798250);
    path_14.cubicTo(
        size.width * 0.2214155,
        size.height * 0.3776737,
        size.width * 0.2223463,
        size.height * 0.3756051,
        size.width * 0.2232358,
        size.height * 0.3736192);
    path_14.cubicTo(
        size.width * 0.2241253,
        size.height * 0.3716334,
        size.width * 0.2250147,
        size.height * 0.3697820,
        size.width * 0.2258732,
        size.height * 0.3679926);
    path_14.cubicTo(
        size.width * 0.2267316,
        size.height * 0.3662033,
        size.width * 0.2275797,
        size.height * 0.3644967,
        size.width * 0.2284071,
        size.height * 0.3628936);
    path_14.cubicTo(
        size.width * 0.2292345,
        size.height * 0.3612904,
        size.width * 0.2300413,
        size.height * 0.3596872,
        size.width * 0.2308273,
        size.height * 0.3581978);
    path_14.cubicTo(
        size.width * 0.2316134,
        size.height * 0.3567085,
        size.width * 0.2323890,
        size.height * 0.3552811,
        size.width * 0.2331441,
        size.height * 0.3538952);
    path_14.cubicTo(
        size.width * 0.2338991,
        size.height * 0.3525092,
        size.width * 0.2345920,
        size.height * 0.3512267,
        size.width * 0.2353264,
        size.height * 0.3499752);
    path_14.cubicTo(
        size.width * 0.2360607,
        size.height * 0.3487237,
        size.width * 0.2367123,
        size.height * 0.3475342,
        size.width * 0.2373949,
        size.height * 0.3463862);
    path_14.cubicTo(
        size.width * 0.2380775,
        size.height * 0.3452381,
        size.width * 0.2387188,
        size.height * 0.3441521,
        size.width * 0.2393393,
        size.height * 0.3431074);
    path_14.cubicTo(
        size.width * 0.2399599,
        size.height * 0.3420628,
        size.width * 0.2405908,
        size.height * 0.3410388,
        size.width * 0.2411700,
        size.height * 0.3400873);
    path_14.arcToPoint(Offset(size.width * 0.2429903, size.height * 0.3373154),
        radius: Radius.elliptical(
            size.width * 0.02171957, size.height * 0.02172024),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2454208, size.height * 0.3352985),
        radius: Radius.elliptical(
            size.width * 0.008470632, size.height * 0.008470895),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2513885, size.height * 0.3345848),
        radius: Radius.elliptical(
            size.width * 0.008274122, size.height * 0.008274378),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2562702, size.height * 0.3381738),
        radius: Radius.elliptical(
            size.width * 0.007994870, size.height * 0.007995118),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2573769, size.height * 0.3440900),
        radius: Radius.elliptical(
            size.width * 0.008377548, size.height * 0.008377808),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.arcToPoint(Offset(size.width * 0.2561668, size.height * 0.3468723),
        radius: Radius.elliptical(
            size.width * 0.007529451, size.height * 0.007529684),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_14.close();

    // Paint paint_14_stroke = Paint()
    //   ..style = PaintingStyle.stroke
    //   ..strokeWidth = 1;
    // paint_14_stroke.color = lineColor;
    // paint_14_stroke.strokeCap = StrokeCap.round;
    // paint_14_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_14, _whizPuzzle.paint);

    Path path_1 = Path();
    path_1.moveTo(size.width * 0.3457859, size.height * 0.9126846);
    path_1.arcToPoint(Offset(size.width * 0.3457859, size.height * 0.9159116),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_1.arcToPoint(Offset(size.width * 0.3441621, size.height * 0.9165839),
        radius: Radius.elliptical(
            size.width * 0.002244355, size.height * 0.002244425),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_1.lineTo(size.width * 0.1088668, size.height * 0.9165839);
    path_1.arcToPoint(Offset(size.width * 0.1065293, size.height * 0.9143188),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_1.arcToPoint(Offset(size.width * 0.1072016, size.height * 0.9126846),
        radius: Radius.elliptical(
            size.width * 0.002358125, size.height * 0.002358198),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_1.lineTo(size.width * 0.1213814, size.height * 0.8985147);
    path_1.arcToPoint(Offset(size.width * 0.1229948, size.height * 0.8978528),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_1.lineTo(size.width * 0.3299306, size.height * 0.8978528);
    path_1.arcToPoint(Offset(size.width * 0.3315441, size.height * 0.8985147),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_1.close();

    Paint paint_1_stroke = leftBottom.paint;
    // Paint paint_1_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_1_stroke.color = ledColor.withOpacity(0.0);
    paint_1_stroke.strokeCap = StrokeCap.round;
    paint_1_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_1, paint_1_stroke);
    //選擇圓點
    if (leftBottom.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4, size.height / 4 * 3),
          size.width / 8,
          paint_1_stroke
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[1].index > PressureStatus.NORMAL.index) {
      paint_1_stroke..style = ui.PaintingStyle.fill;
      paint_1_stroke..color = Colors.blue.withOpacity(0.5);
      // switch (_whizPuzzle.sensorValue[1]) {
      //   case PressureStatus.P1:
      //     paint_1_stroke..color = Colors.blue.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P2:
      //     paint_1_stroke..color = Colors.orange.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P3:
      //     paint_1_stroke..color = Colors.red.withOpacity(0.5);
      //     break;
      //   default:
      // }
      canvas.drawCircle(Offset(size.width / 4, size.height / 4 * 3),
          size.width / 8, paint_1_stroke);
    }

    Path path_2 = Path();
    path_2.moveTo(size.width * 0.08340315, size.height * 0.6558169);
    path_2.arcToPoint(Offset(size.width * 0.08730233, size.height * 0.6542034),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_2.lineTo(size.width * 0.1014718, size.height * 0.6683733);
    path_2.arcToPoint(Offset(size.width * 0.1021440, size.height * 0.6699868),
        radius: Radius.elliptical(
            size.width * 0.002306411, size.height * 0.002306483),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_2.lineTo(size.width * 0.1021440, size.height * 0.8769186);
    path_2.arcToPoint(Offset(size.width * 0.1014718, size.height * 0.8785321),
        radius: Radius.elliptical(
            size.width * 0.002306411, size.height * 0.002306483),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_2.lineTo(size.width * 0.08730233, size.height * 0.8926917);
    path_2.arcToPoint(Offset(size.width * 0.08340315, size.height * 0.8910885),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_2.close();

    // Paint paint_2_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_2_stroke.color = ledColor.withOpacity(1.0);
    Paint paint_2_stroke = leftBottom.paint;
    paint_2_stroke.strokeCap = StrokeCap.round;
    paint_2_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_2, paint_2_stroke);

    Path path_3 = Path();
    path_3.moveTo(size.width * 0.6557862, size.height * 0.9165839);
    path_3.arcToPoint(Offset(size.width * 0.6541831, size.height * 0.9126846),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_3.lineTo(size.width * 0.6683422, size.height * 0.8985147);
    path_3.arcToPoint(Offset(size.width * 0.6699660, size.height * 0.8978528),
        radius: Radius.elliptical(
            size.width * 0.002316754, size.height * 0.002316826),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_3.lineTo(size.width * 0.8768811, size.height * 0.8978528);
    path_3.arcToPoint(Offset(size.width * 0.8784945, size.height * 0.8985147),
        radius: Radius.elliptical(
            size.width * 0.002296069, size.height * 0.002296140),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_3.lineTo(size.width * 0.8926640, size.height * 0.9126846);
    path_3.arcToPoint(Offset(size.width * 0.8926640, size.height * 0.9159116),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_3.arcToPoint(Offset(size.width * 0.8910402, size.height * 0.9165839),
        radius: Radius.elliptical(
            size.width * 0.002223670, size.height * 0.002223739),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_3.close();

    // Paint paint_3_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_3_stroke.color = ledColor.withOpacity(1.0);
    Paint paint_3_stroke = rightBottom.paint;
    paint_3_stroke.strokeCap = StrokeCap.round;
    paint_3_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_3, paint_3_stroke);
    //圓點
    if (rightBottom.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4 * 3, size.height / 4 * 3),
          size.width / 8,
          paint_3_stroke
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[2].index > PressureStatus.NORMAL.index) {
      paint_3_stroke..style = ui.PaintingStyle.fill;
      paint_3_stroke..color = Colors.blue.withOpacity(0.5);
      // switch (_whizPuzzle.sensorValue[2]) {
      //   case PressureStatus.P1:
      //     paint_3_stroke..color = Colors.blue.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P2:
      //     paint_3_stroke..color = Colors.orange.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P3:
      //     paint_3_stroke..color = Colors.red.withOpacity(0.5);
      //     break;
      //   default:
      // }
      canvas.drawCircle(Offset(size.width / 4 * 3, size.height / 4 * 3),
          size.width / 8, paint_3_stroke);
    }

    Path path_4 = Path();
    path_4.moveTo(size.width * 0.9165658, size.height * 0.3441831);
    path_4.arcToPoint(Offset(size.width * 0.9126666, size.height * 0.3457966),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_4.lineTo(size.width * 0.8985076, size.height * 0.3316267);
    path_4.arcToPoint(Offset(size.width * 0.8978353, size.height * 0.3300029),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_4.lineTo(size.width * 0.8978353, size.height * 0.1230814);
    path_4.arcToPoint(Offset(size.width * 0.8985076, size.height * 0.1214679),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_4.lineTo(size.width * 0.9126666, size.height * 0.1072980);
    path_4.arcToPoint(Offset(size.width * 0.9165658, size.height * 0.1089218),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_4.close();

    // Paint paint_4_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_4_stroke.color = ledColor.withOpacity(1.0);
    Paint paint_4_stroke = rightTop.paint;
    paint_4_stroke.strokeCap = StrokeCap.round;
    paint_4_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_4, paint_4_stroke);
    //選擇圓點
    if (rightTop.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4 * 3, size.height / 4),
          size.width / 8,
          paint_4_stroke
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[3].index > PressureStatus.NORMAL.index) {
      paint_4_stroke..style = ui.PaintingStyle.fill;
      paint_4_stroke..color = Colors.blue.withOpacity(0.5);
      switch (_whizPuzzle.sensorValue[3]) {
        case PressureStatus.P1:
          paint_4_stroke..color = Colors.blue.withOpacity(0.5);
          break;
        case PressureStatus.P2:
          paint_4_stroke..color = Colors.orange.withOpacity(0.5);
          break;
        case PressureStatus.P3:
          paint_4_stroke..color = Colors.red.withOpacity(0.5);
          break;
        default:
      }
      canvas.drawCircle(Offset(size.width / 4 * 3, size.height / 4),
          size.width / 8, paint_4_stroke);
    }

    Path path_5 = Path();
    path_5.moveTo(size.width * 0.3441931, size.height * 0.08340573);
    path_5.arcToPoint(Offset(size.width * 0.3464892, size.height * 0.08568119),
        radius: Radius.elliptical(
            size.width * 0.002296069, size.height * 0.002296140),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_5.arcToPoint(Offset(size.width * 0.3458169, size.height * 0.08730503),
        radius: Radius.elliptical(
            size.width * 0.002265041, size.height * 0.002265111),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_5.lineTo(size.width * 0.3316475, size.height * 0.1014853);
    path_5.arcToPoint(Offset(size.width * 0.3300340, size.height * 0.1021472),
        radius: Radius.elliptical(
            size.width * 0.002316754, size.height * 0.002316826),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_5.lineTo(size.width * 0.1230776, size.height * 0.1021472);
    path_5.arcToPoint(Offset(size.width * 0.1214538, size.height * 0.1014853),
        radius: Radius.elliptical(
            size.width * 0.002316754, size.height * 0.002316826),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_5.lineTo(size.width * 0.1072843, size.height * 0.08730503);
    path_5.arcToPoint(Offset(size.width * 0.1072843, size.height * 0.08408837),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_5.arcToPoint(Offset(size.width * 0.1089185, size.height * 0.08340573),
        radius: Radius.elliptical(
            size.width * 0.002316754, size.height * 0.002316826),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_5.close();

    // Paint paint_5_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_5_stroke.color = ledColor.withOpacity(1.0);
    Paint paint_5_stroke = leftTop.paint;
    paint_5_stroke.strokeCap = StrokeCap.round;
    paint_5_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_5, paint_5_stroke);
    //選擇圓點
    if (leftTop.selected && !_whizPuzzle.selected)
      canvas.drawCircle(
          Offset(size.width / 4, size.height / 4),
          size.width / 8,
          paint_5_stroke
            ..color = Colors.grey.withOpacity(0.5)
            ..style = ui.PaintingStyle.fill);
    //感測器
    if (_whizPuzzle.sensorValue[0].index > PressureStatus.NORMAL.index) {
      paint_5_stroke..style = ui.PaintingStyle.fill;
      paint_5_stroke..color = Colors.blue.withOpacity(0.5);
      // switch (_whizPuzzle.sensorValue[0]) {
      //   case PressureStatus.P1:
      //     paint_5_stroke..color = Colors.blue.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P2:
      //     paint_5_stroke..color = Colors.orange.withOpacity(0.5);
      //     break;
      //   case PressureStatus.P3:
      //     paint_5_stroke..color = Colors.red.withOpacity(0.5);
      //     break;
      //   default:
      // }
      canvas.drawCircle(Offset(size.width / 4, size.height / 4), size.width / 8,
          paint_5_stroke);
    }

    Path path_11 = Path();
    path_11.moveTo(size.width * 0.9126563, size.height * 0.6542034);
    path_11.arcToPoint(Offset(size.width * 0.9165555, size.height * 0.6558169),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.lineTo(size.width * 0.9165555, size.height * 0.8910988);
    path_11.arcToPoint(Offset(size.width * 0.9142801, size.height * 0.8933846),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.arcToPoint(Offset(size.width * 0.9126563, size.height * 0.8927123),
        radius: Radius.elliptical(
            size.width * 0.002327097, size.height * 0.002327169),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.lineTo(size.width * 0.8984972, size.height * 0.8785425);
    path_11.arcToPoint(Offset(size.width * 0.8978249, size.height * 0.8769290),
        radius: Radius.elliptical(
            size.width * 0.002244355, size.height * 0.002244425),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.lineTo(size.width * 0.8978249, size.height * 0.6700074);
    path_11.arcToPoint(Offset(size.width * 0.8984972, size.height * 0.6683836),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_11.close();

    // Paint paint_11_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_11_stroke.color = ledColor.withOpacity(1.0);
    Paint paint_11_stroke = rightBottom.paint;
    paint_11_stroke.strokeCap = StrokeCap.round;
    paint_11_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_11, paint_11_stroke);

    Path path_12 = Path();
    path_12.moveTo(size.width * 0.6541831, size.height * 0.08730503);
    path_12.arcToPoint(Offset(size.width * 0.6557862, size.height * 0.08340573),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.lineTo(size.width * 0.8910608, size.height * 0.08340573);
    path_12.arcToPoint(Offset(size.width * 0.8933569, size.height * 0.08568119),
        radius: Radius.elliptical(
            size.width * 0.002296069, size.height * 0.002296140),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.arcToPoint(Offset(size.width * 0.8926846, size.height * 0.08730503),
        radius: Radius.elliptical(
            size.width * 0.002265041, size.height * 0.002265111),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.lineTo(size.width * 0.8785152, size.height * 0.1014853);
    path_12.arcToPoint(Offset(size.width * 0.8768914, size.height * 0.1021472),
        radius: Radius.elliptical(
            size.width * 0.002316754, size.height * 0.002316826),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.lineTo(size.width * 0.6699763, size.height * 0.1021472);
    path_12.arcToPoint(Offset(size.width * 0.6683629, size.height * 0.1014853),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_12.close();

    // Paint paint_12_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_12_stroke.color = ledColor.withOpacity(1.0);
    Paint paint_12_stroke = rightTop.paint;
    paint_12_stroke.strokeCap = StrokeCap.round;
    paint_12_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_12, paint_12_stroke);

    Path path_13 = Path();
    path_13.moveTo(size.width * 0.08730233, size.height * 0.3457966);
    path_13.arcToPoint(Offset(size.width * 0.08340315, size.height * 0.3441831),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.08340315, size.height * 0.1089115);
    path_13.arcToPoint(Offset(size.width * 0.08730233, size.height * 0.1072980),
        radius: Radius.elliptical(
            size.width * 0.002285726, size.height * 0.002285797),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.1014718, size.height * 0.1214679);
    path_13.arcToPoint(Offset(size.width * 0.1021440, size.height * 0.1230710),
        radius: Radius.elliptical(
            size.width * 0.002275383, size.height * 0.002275454),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.lineTo(size.width * 0.1021440, size.height * 0.3300029);
    path_13.arcToPoint(Offset(size.width * 0.1014718, size.height * 0.3316164),
        radius: Radius.elliptical(
            size.width * 0.002306411, size.height * 0.002306483),
        rotation: 0,
        largeArc: false,
        clockwise: true);
    path_13.close();

    // Paint paint_13_stroke = Paint()
    //   ..style = PaintingStyle.fill
    //   ..strokeWidth = 1;
    // paint_13_stroke.color = ledColor.withOpacity(1.0);
    Paint paint_13_stroke = leftTop.paint;
    paint_13_stroke.strokeCap = StrokeCap.round;
    paint_13_stroke.strokeJoin = StrokeJoin.round;
    canvas.drawPath(path_13, paint_13_stroke);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
