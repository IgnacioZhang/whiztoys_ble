import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';

import 'check_sum.dart';
import 'color_define.dart';

part 'whiztoys.g.dart';

enum PressureStatus { EMPTY, NORMAL, P1, P2, P3, P4, P5, P6, P7, P8, P9 }

class WhizToysData {
  static bool isDebug = false;

  static Uint8List generateLayout(int row, int col) {
    Uint8List result = Uint8List(1 + row * col);
    var rowAndCol = ((row << 4) & 0xff) | (col & 0xff);
    result[0] = rowAndCol;
    for (int i = 1; i < row * col + 1; i++) {
      result[i] = int.parse("01011010", radix: 2);
    }
    return Checksum.generate(result);
  }

  //packet[0] 一個byte代表Layout row col， 00010001，1*1片巧拼陣列，實際為2*2sensor Layout
  static WhizToysLayout parseLayout(Uint8List packet) {
    if (packet.length == 0) {
      return WhizToysLayout(
          sensorRow: 0, sensorCol: 0, sensorLayoutArray: [[]]);
    }
    if (!Checksum.check(packet, packet.length)) {
      return WhizToysLayout(
          sensorRow: 0, sensorCol: 0, sensorLayoutArray: [[]]);
    }
    int sensorRow = (((packet[0] & 0xff) >> 4) & 0x0f) * 2;
    int sensorCol = ((packet[0] & 0xff) & 0x0f) * 2;
    //print("sensorRow:$sensorRow, sensorCol:$sensorCol");
    Uint8List layoutPacket = packet.sublist(1, packet.length - 1);
    //print("layoutPacket :$layoutPacket");
    List<List<PressureStatus>> sensorLayout = List.generate(
        sensorRow,
        (index) =>
            List.filled(sensorCol, PressureStatus.NORMAL, growable: false),
        growable: false);
    //解資料
    String debugBufStr = "";
    int r = 0, c = 0;
    for (var b in layoutPacket) {
      for (int j = 0; j < 8; j += 2) {
        if (c == sensorCol) {
          if (isDebug) debugBufStr = "";
          r++;
          c = 0;
        }
        if (r == sensorRow) break;
        /*
        一個byte儲存4個sensor信息，01010110, 
        value= 0 ,PressureStatus.EMPTY
        value>0 ,PressureStatus.NORMAL
        value =2 ,PressureStatus.P1
        TODO: value =3 , PressureStatus.P2
        col = 2, row = 2 
        r  c   b           j   value
        0  0   01010110    0   01 => 0*2+1 = 1
        0  1   01010110    2   01 => 0*2+1 = 1
        1  0   01010110    4   01 => 0*2+1 = 1
        1  1   01010110    6   10 => 1*2+0 = 2
        [[1,1],
         [1,2]]

        col = 4, row = 2
        r  c   b           j   value
        0  0   01010110    0   01 => 0*2+1 = 1
        0  1   01010110    2   01 => 0*2+1 = 1
        0  2   01010110    4   01 => 0*2+1 = 1
        0  3   01010110    6   10 => 1*2+0 = 2
        1  0   01010110    0   01 => 0*2+1 = 1
        1  1   01010110    2   01 => 0*2+1 = 1
        1  2   01010110    4   01 => 0*2+1 = 1
        1  3   01010110    6   10 => 1*2+0 = 2
        [[1,1,1,2],
         [1,1,1,2]]
        */
        int value = (((b & 0xff) >> (7 - j)) & 0x01) * 2 +
            (((b & 0xff) >> (7 - (j + 1))) & 0x01);
        if (isDebug) debugBufStr += value.toString();
        if (isDebug) print("debugBufStr :$debugBufStr");
        sensorLayout[r][c] = value == 2
            ? PressureStatus.P1
            : value > 0
                ? PressureStatus.NORMAL
                : PressureStatus.EMPTY;
        c++;
      }
    }
    //debug string

    return WhizToysLayout(
        sensorRow: sensorRow,
        sensorCol: sensorCol,
        sensorLayoutArray: sensorLayout);
  }

  //產生一個感測器的數值
  static Uint8List generatSensorPacket(row, col,
      {leftTop = false,
      leftBottom = false,
      rightBottom = false,
      rightTop = false}) {
    dynamic rowAndCol = ((row << 4) & 0xff) | (col & 0xff);
    int sensors = 0;
    sensors = sensors | (leftTop ? 0x08 : 0x00);
    sensors = sensors | (leftBottom ? 0x04 : 0x00);
    sensors = sensors | (rightBottom ? 0x02 : 0x00);
    sensors = sensors | (rightTop ? 0x01 : 0x00);
    return Uint8List.fromList([rowAndCol, sensors]);
  }

  //藍牙接收到的格式為 [位置，sensors，位置，sensors]
  ///有分是否為灰階版本控制盒
  static List<WhizToysSensor> parseSensor(Uint8List packet, bool isGrayLevel) {
    assert(packet.length % 2 == 0);
    List<WhizToysSensor?> sensors = List.filled(packet.length ~/ 2, null);
    for (int i = 0; i < packet.length ~/ 2; i++) {
      int row = ((packet[i * 2] & 0xff) >> 4) & 0x0f,
          col = (packet[i * 2] & 0xff) & 0x0f;
      sensors[i] = WhizToysSensor(
          row: row,
          col: col,
          packet: (packet[i * 2 + 1] & 0xff),
          isGrayLevel: isGrayLevel);
    }
    return List<WhizToysSensor>.from(sensors);
  }
}

class WhizToysLayout {
  WhizToysLayout(
      {required this.sensorRow,
      required this.sensorCol,
      required this.sensorLayoutArray,
      stepCount});

  int sensorRow, sensorCol;
  List<List<PressureStatus>> sensorLayoutArray;
  int stepCount = 0;

  void updateSensor(List<WhizToysSensor> sensors) {
    for (WhizToysSensor sensor in sensors) {
      if (sensor.row >= this.sensorRow || sensor.col >= this.sensorCol) return;
      // //print("sensor.col * 2:${sensor.col * 2} ${sensor.sensorValues[0]}");
      // 巧拼反應測試
      if (sensorLayoutArray[sensor.row * 2][sensor.col * 2] ==
          PressureStatus.NORMAL) {
        if (sensor.sensorValues[0] != PressureStatus.NORMAL) {
          stepCount++;
          print("x = ");
          print(sensor.row * 2);
          print("y = ");
          print(sensor.col * 2);
          print("stepCount = ");
          print(stepCount);
        }
        sensorLayoutArray[sensor.row * 2][sensor.col * 2] =
            sensor.sensorValues[0];
      } else {
        sensorLayoutArray[sensor.row * 2][sensor.col * 2] =
            sensor.sensorValues[0];
      }

      if (sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2] ==
          PressureStatus.NORMAL) {
        if (sensor.sensorValues[1] != PressureStatus.NORMAL) {
          stepCount++;
          print("x = ");
          print(sensor.row * 2);
          print("y = ");
          print(sensor.col * 2);
          print("stepCount = ");
          print(stepCount);
        }
        sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2] =
            sensor.sensorValues[1];
      } else {
        sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2] =
            sensor.sensorValues[1];
      }

      if (sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2 + 1] ==
          PressureStatus.NORMAL) {
        if (sensor.sensorValues[2] != PressureStatus.NORMAL) {
          stepCount++;
          print("x = ");
          print(sensor.row * 2);
          print("y = ");
          print(sensor.col * 2);
          print("stepCount = ");
          print(stepCount);
        }
        sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2 + 1] =
            sensor.sensorValues[2];
      } else {
        sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2 + 1] =
            sensor.sensorValues[2];
      }

      if (sensorLayoutArray[sensor.row * 2][sensor.col * 2 + 1] ==
          PressureStatus.NORMAL) {
        if (sensor.sensorValues[3] != PressureStatus.NORMAL) {
          stepCount++;
          print("x = ");
          print(sensor.row * 2);
          print("y = ");
          print(sensor.col * 2);
          print("stepCount = ");
          print(stepCount);
        }
        sensorLayoutArray[sensor.row * 2][sensor.col * 2 + 1] =
            sensor.sensorValues[3];
      } else {
        sensorLayoutArray[sensor.row * 2][sensor.col * 2 + 1] =
            sensor.sensorValues[3];
      }
      // 斌權的code
      // //print("layoutArray: $layoutArray");
      // sensorLayoutArray[sensor.row * 2][sensor.col * 2] =
      //     sensor.sensorValues[0];
      // sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2] =
      //     sensor.sensorValues[1];
      // sensorLayoutArray[sensor.row * 2 + 1][sensor.col * 2 + 1] =
      //     sensor.sensorValues[2];
      // sensorLayoutArray[sensor.row * 2][sensor.col * 2 + 1] =
      //     sensor.sensorValues[3];
    }

    // //print("layoutArray :$layoutArray");
    // postInvalidate();
  }
}

//用來處理控制盒notify的數值，第一個byte代表row col，第二個byte 00001111 代表sensor數值
class WhizToysSensor {
  WhizToysSensor(
      {required this.row,
      required this.col,
      this.packet,
      this.isGrayLevel = false}) {
    if (packet != null) {
      late int s0, s1, s2, s3;
      if (isGrayLevel) {
        s0 = ((packet! >> 6) & 0x03) + 1; //left top
        s1 = ((packet! >> 4) & 0x03) + 1; //leftBottom
        s2 = ((packet! >> 2) & 0x03) + 1; //rightBottom
        s3 = ((packet!) & 0x03) + 1; //rightTop
      } else {
        s0 = ((packet! >> 3) & 0x01) + 1; //left top
        s1 = ((packet! >> 2) & 0x01) + 1; //leftBottom
        s2 = ((packet! >> 1) & 0x01) + 1; //rightBottom
        s3 = ((packet!) & 0x01) + 1; //rightTop
      }

      this.sensorValues = [
        PressureStatus.values[s0],
        PressureStatus.values[s1],
        PressureStatus.values[s2],
        PressureStatus.values[s3]
      ];
    } else {
      this.sensorValues = [
        PressureStatus.EMPTY,
        PressureStatus.EMPTY,
        PressureStatus.EMPTY,
        PressureStatus.EMPTY
      ];
    }
  }

  bool isGrayLevel;

  ///
  ///sensorValues [leftTop,leftBottom,rightBottom,rightTop]
  WhizToysSensor.fromValue(
      this.row, this.col, List<PressureStatus> sensorValues,
      {this.isGrayLevel = false})
      : this.sensorValues = sensorValues;

  int row, col;

  ///sensorValues [leftTop,leftBottom,rightBottom,rightTop]
  late List<PressureStatus> sensorValues;
  int? packet;
  //0+1 = 1

  int get sensorRow => row * 2;
  int get sensorCol => col * 2;
  PressureStatus get leftTop => sensorValues[0];
  PressureStatus get leftBottom => sensorValues[1];
  PressureStatus get rightBottom => sensorValues[2];
  PressureStatus get rightTop => sensorValues[3];

  bool isSensorPressed() {
    for (PressureStatus pressure in sensorValues) {
      if (pressure != PressureStatus.EMPTY && pressure != PressureStatus.NORMAL)
        return true;
    }
    return false;
  }

  @override
  String toString() {
    return "WhizToysSensors{row:$row,col:$col,sensorValues:$sensorValues}";
  }
}

enum LocationMode { all, leftTop, leftBottom, rightBottom, rightTop, allSep }

enum LedMode { notSetting, none, basic, blink, breath, loopLed, colorful }

// [length，location，mode，R，G，B]
@JsonSerializable()
class WhizToysLed {
  WhizToysLed(this.row, this.col, this.mode, this.R, this.G, this.B);

  @JsonKey(name: "r")
  int row;
  @JsonKey(name: "c")
  int col;
  @JsonKey(name: "m")
  int mode;
  int R, G, B;

  get locationMode => mode >> 5;

  //設定feedback方法
  factory WhizToysLed.feedback(
      {required int sensorRow,
      required int sensorCol,
      bool controlAllLed = false,
      bool controlAllLedSep = false,
      required bool showLedImmediately,
      required LedMode ledMode,
      required bool showLedLong,
      required int R,
      required int G,
      required int B}) {
    int _row = sensorRow ~/ 2;
    int _col = sensorCol ~/ 2;
    // //print("row: $_row,col: $_col");
    LocationMode locationMode;
    if (controlAllLed) {
      locationMode = LocationMode.all;
    } else if (controlAllLedSep) {
      locationMode = LocationMode.allSep;
    } else {
      int rowBottom = sensorRow % 2;
      int colRight = sensorCol % 2;
      if (rowBottom == 1 && colRight == 1) {
        //rightBottom
        locationMode = LocationMode.rightBottom;
      } else if (rowBottom == 1) {
        //leftBottom
        locationMode = LocationMode.leftBottom;
      } else if (colRight == 1) {
        //rightTop
        locationMode = LocationMode.rightTop;
      } else {
        //leftTop
        locationMode = LocationMode.leftTop;
      }
    }
    int value1 = (locationMode.index & 0xff) << 5; //可能還需要 只取3bits
    int value2 = ((showLedImmediately ? 1 : 0) & 0xff) << 4;
    int value3 = (ledMode.index & 0xff) << 1;
    int value4 = ((showLedLong ? 1 : 0) & 0xff);
    int modeInfo = (value1 | value2 | value3 | value4);
    return WhizToysLed(_row, _col, modeInfo, R, G, B);
  }

  factory WhizToysLed.fromSensorRowCol(
      int sensorRow, int sensorCol, int R, int G, int B) {
    int row = sensorRow ~/ 2;
    int col = sensorCol ~/ 2;
    int rowBottom = sensorRow % 2;
    int colRight = sensorCol % 2;
    // LedMode ledMode = LedMode.leftTop; //leftTop
    // if (rowBottom == 1 && colRight == 1) {
    //   //rightBottom
    //   ledMode = LedMode.rightBottom;
    // } else if (rowBottom == 1) {
    //   //leftBottom
    //   ledMode = LedMode.leftBottom;
    // } else if (colRight == 1) {
    //   //rightTop
    //   ledMode = LedMode.rightTop;
    // }
    return WhizToysLed(row, col, 0, R, G, B);
  }

  List<int> get ledPacket {
    // //print("row: $row,col: $col mode: $mode,R:$R,G:$G,B:$B");
    dynamic rowAndCol = ((row << 4) & 0xff) | (col & 0xff);

    return [rowAndCol, mode, parseColorToColorNum(R, G, B)]; //已經改為數字代表顏色
  }

  factory WhizToysLed.fromJson(Map<String, dynamic> json) =>
      _$WhizToysLedFromJson(json);
  Map<String, dynamic> toJson() => _$WhizToysLedToJson(this);

  @override
  String toString() {
    return "${this.toJson()}";
  }
}

parseColorToColorNum(int R, int G, int B) {
  int? index;
  int? finalDiffernt;

  colorList.forEach((color) {
    int _differnt =
        ((color[0] - R).abs() + (color[1] - G).abs() + (color[2] - B).abs());
    if (finalDiffernt == null) {
      finalDiffernt = _differnt;
      index = 0;
    } else if (_differnt < finalDiffernt!) {
      finalDiffernt = _differnt;
      index = colorList.indexOf(color);
    }
  });
  return index;
}
