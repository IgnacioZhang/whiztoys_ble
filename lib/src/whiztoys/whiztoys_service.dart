import 'dart:async';

import 'package:flutter_blue_plus/flutter_blue_plus.dart';
import 'package:rxdart/rxdart.dart';
import 'whiztoys.dart';
import 'dart:typed_data';

class WhizToysService {
  WhizToysService._();

  static WhizToysService _instance = WhizToysService._();
  static WhizToysService get instance => _instance;

  BluetoothDevice? device;

  BluetoothCharacteristic? characteristicWriteLed,
      characteristicRead,
      characteristicNotify,
      characteristicResetSensor;

  WhizToysLayout? _whizToysLayout;

  final layout = BehaviorSubject<WhizToysLayout>();
  get layoutStream => layout.stream;

  final sensor = BehaviorSubject<WhizToysSensor>();
  ValueStream<WhizToysSensor> get sensorStream => sensor.stream;

  // ignore: close_sinks
  final sensors = BehaviorSubject<List<WhizToysSensor>>();
  ValueStream<List<WhizToysSensor>> get sensorsStream => sensors.stream;

  final ledControlSubject = PublishSubject();

  final ledControlSubjectWindow = PublishSubject<List<int>>();

  final resetSensorControlSubject = PublishSubject();

  final _deviceConnectionState = BehaviorSubject<BluetoothDeviceState>();
  ValueStream<BluetoothDeviceState> get deviceState =>
      _deviceConnectionState.stream;

  StreamSubscription? _notifySubscription,
      ledSubscription,
      ledSubscriptionWindow,
      _connectionSubscription,
      _resetSensorSubscription;

  bool isGraylevel = false;

  //TODO: 修改WTS1 and WTS2
  Future scanForWTS1() async {
    List<BluetoothDevice> connectedDevices =
        await FlutterBluePlus.instance.connectedDevices;

    //print(connectedDevices);
    if (connectedDevices.where((device) => device.name == "WTS1").length != 0) {
      //print("有WTS1連線中");
      await connectedDevices.firstWhere((element) {
        return element.name == "WTS1";
      }).disconnect();
      //print("斷開連線");
    }

    await FlutterBluePlus.instance
        .startScan(timeout: Duration(seconds: 5))
        .then((scanResult) {
      List<ScanResult> result = List<ScanResult>.from(scanResult);
      //print(result);
      ScanResult? wts1ScanResult;
      try {
        wts1ScanResult = result.firstWhere(
          (element) {
            return element.device.name.contains('WTS');
          },
        );
      } catch (e) {}

      if (wts1ScanResult == null) {
        //print("wts1ScanResult ==null");
        return;
      }

      this.device = wts1ScanResult.device;
      return;
    });

    if (this.device != null) await connect();
  }

  Future connect() async {
    if (device == null) return;
    _connectionSubscription = device!.state.listen((event) {
      //print('[WhizToysService] connect event $event');
      _deviceConnectionState.add(event);
      if (event == BluetoothDeviceState.disconnected) {
        characteristicRead = null;
        characteristicNotify = null;
        characteristicWriteLed = null;
        characteristicResetSensor = null;
        _notifySubscription?.cancel();
        ledSubscription?.cancel();
        ledSubscriptionWindow?.cancel();
        _resetSensorSubscription?.cancel();
      } else if (event == BluetoothDeviceState.connected) {
        if (device!.name == "WTS1") {
          isGraylevel = false;
        } else {
          isGraylevel = true;
        }
        discoverService();
      }
    });
    await device!.connect(autoConnect: true);
  }

  disconnect() async {
    if (device == null) return;
    await device!.disconnect();
    _deviceConnectionState.add(BluetoothDeviceState.disconnected);
    characteristicRead = null;
    characteristicNotify = null;
    characteristicWriteLed = null;
    characteristicResetSensor = null;
    _notifySubscription?.cancel();
    ledSubscription?.cancel();
    ledSubscriptionWindow?.cancel();
    _connectionSubscription?.cancel();
    _resetSensorSubscription?.cancel();
  }

  discoverService() async {
    //print('[WhizToysService]:discoverService ');
    if (device == null) return;

    List<BluetoothService> bleServices = await device!.discoverServices();
    for (BluetoothService service in bleServices) {
      for (BluetoothCharacteristic characteristic in service.characteristics) {
        if (characteristic.uuid ==
            Guid('0000fee1-0000-1000-8000-00805f9b34fb')) {
          characteristicRead = characteristic;
        }
        if (characteristic.uuid ==
            Guid('0000fee2-0000-1000-8000-00805f9b34fb')) {
          characteristicNotify = characteristic;
        }
        if (characteristic.uuid ==
            Guid('0000fee3-0000-1000-8000-00805f9b34fb')) {
          characteristicWriteLed = characteristic;
        }
        if (characteristic.uuid ==
            Guid('0000fee4-0000-1000-8000-00805f9b34fb')) {
          characteristicResetSensor = characteristic;
        }
      }
    }
    await readLayout();
    await _setNotify(characteristicNotify);
    _setLedWrite();
    _setResetSensorWrite();
  }

  readLayout() async {
    //print('[WhizToysService] readLayout');
    if (characteristicRead == null) return discoverService();

    _whizToysLayout = WhizToysData.parseLayout(
        Uint8List.fromList(await characteristicRead!.read()));
    layout.add(_whizToysLayout!);
  }

  _setNotify(BluetoothCharacteristic? characteristic) async {
    if (characteristic == null) return;
    if (characteristic.isNotifying == false) {
      //print('await setNotifyValue');
      await characteristic.setNotifyValue(true);
      _notifySubscription = characteristic.value.listen((packet) {
        // do something with new value  也就是喂到bloc

        if (packet.length > 0)
          notifySensorChange(WhizToysData.parseSensor(
              Uint8List.fromList(packet), isGraylevel));
      });
    }
  }

  _setLedWrite() async {
    ledSubscription = ledControlSubject
        .interval(Duration(milliseconds: 100))
        .listen((value) async {
      if (value == null) {
        return;
      }
      if (characteristicWriteLed == null) return;
      await characteristicWriteLed!.write(value, withoutResponse: true);
    });

    ledSubscriptionWindow = ledControlSubjectWindow.listen((value) async {
      if (value == null) {
        return;
      }
      if (characteristicWriteLed == null) return;
      await characteristicWriteLed!.write(value, withoutResponse: true);
    });
  }

  _setResetSensorWrite() async {
    _resetSensorSubscription = resetSensorControlSubject
        .interval(Duration(milliseconds: 100))
        .listen((value) async {
      if (value == null) {
        return;
      }
      if (characteristicResetSensor == null) return;
      await characteristicResetSensor!.write(value, withoutResponse: true);
    });
  }

  ///設定感測靈敏度//16進位(App)->10進位(巧拼)
  void resetSensor(int level) {
    // resetSensorControlSubject.add([0x01]);
    if (level == 3) {
      //高靈敏30 0x1E,
      resetSensorControlSubject.add([0x01, 0x1E]);
    }
    if (level == 2) {
      //中靈敏50 0x32, 120 0x78
      resetSensorControlSubject.add([0x01, 0x78]);
    }
    if (level == 1) {
      //低靈敏100 0x64, 200 0xC8
      resetSensorControlSubject.add([0x01, 0xC8]);
    }
    print("level:$level");
  }

  notifySensorChange(List<WhizToysSensor> sensors) {
    this.sensors.sink.add(sensors);
    sensors.forEach((element) {
      sensor.add(element);
    });
    _whizToysLayout?.updateSensor(sensors);
    layout.add(_whizToysLayout!);
  }

  generateLayoutTest(layoutRow, layoutCol) {
    dynamic layoutPacket = WhizToysData.generateLayout(layoutRow, layoutCol);
    _whizToysLayout = WhizToysData.parseLayout(layoutPacket);
    layout.add(_whizToysLayout!);
  }

  generateSensorTest() async {
    var value;
    for (int i = 0; i < 50; i++) {
      //print(i);
      if (i % 2 == 0)
        value = WhizToysData.generatSensorPacket(1, 0,
            leftTop: false,
            rightTop: true,
            leftBottom: false,
            rightBottom: true);
      if (i % 2 == 1)
        value = WhizToysData.generatSensorPacket(1, 0,
            leftTop: true,
            rightTop: false,
            leftBottom: true,
            rightBottom: false);

      notifySensorChange(WhizToysData.parseSensor(value, isGraylevel));
      await Future.delayed(Duration(milliseconds: 10));
    }
  }

  setAllCarpetToBasic() {
    if (_whizToysLayout == null) {
      return;
    }
    for (int r = 0; r < _whizToysLayout!.sensorRow; r++) {
      for (int c = 0; c < _whizToysLayout!.sensorCol; c++) {
        if (_whizToysLayout!.sensorLayoutArray[r][c] == PressureStatus.EMPTY)
          continue;
        WhizToysLed data = WhizToysLed.feedback(
            sensorRow: r,
            sensorCol: c,
            controlAllLed: false,
            showLedImmediately: true,
            showLedLong: false,
            ledMode: LedMode.basic,
            R: 0,
            G: 0,
            B: 255);
        ledControlSubject.add(data.ledPacket..insert(0, 0x03));
      }
    }
  }
}
