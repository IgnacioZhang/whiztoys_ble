import 'dart:typed_data';

class Checksum {
  static Uint8List generate(Uint8List data) {
    Uint8List result = Uint8List(data.length + 1);
    int xor = 0;
    for (int i = 0; i < data.length; i++) {
      result[i] = data[i];
      xor = xor ^ data[i];
    }
    result[data.length] = xor;
    return result;
  }

  static bool check(Uint8List packet, int length) {
    int xor = 0;
    for (int i = 0; i < length - 1; i++) {
      xor = xor ^ packet[i];
    }
    //print(xor);
    return xor == packet[length - 1];
  }
}
