// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'whiztoys.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WhizToysLed _$WhizToysLedFromJson(Map<String, dynamic> json) => WhizToysLed(
      json['r'] as int,
      json['c'] as int,
      json['m'] as int,
      json['R'] as int,
      json['G'] as int,
      json['B'] as int,
    );

Map<String, dynamic> _$WhizToysLedToJson(WhizToysLed instance) =>
    <String, dynamic>{
      'r': instance.row,
      'c': instance.col,
      'm': instance.mode,
      'R': instance.R,
      'G': instance.G,
      'B': instance.B,
    };
